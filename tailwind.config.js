/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      screens: {
        '3xl': '1920px'
      },
      colors: {
        blue: '#00a8dd',
        'blue-dark': '#004c85',
        red: '#ed1c24',
        'font-main': '#333',
        'very-light-gray': '#f2f2f2',
        'light-gray': '#e6e6e6',
        'medium-gray': '#808080',
        'medium-gray-2': '#b3b3b3'
      },
      spacing: {
        15: '60px'
      },
      dropShadow: {
        sm: '1px 1px 1px rgba(0, 0, 0, 0.5)'
      },
      padding: {
        30: '30px'
      },
      gap: {
        30: '30px'
      },
      minHeight: {
        4: '1rem',
        5: '1.25rem',
        8: '2rem'
      }
    }
  },
  plugins: [require('daisyui'), require('tailwind-scrollbar-hide')],
  daisyui: {
    themes: [
      {
        light: {
          ...require('daisyui/src/colors/themes')['[data-theme=light]'],
          primary: '#00a8dd',
          'primary-focus': '#004c85',
          secondary: '#004c85',
          error: '#ed1c24',
          'error-content': '#fff',
          neutral: '#333',
          'base-100': '#fff'
        }
      },
      {
        dark: {
          ...require('daisyui/src/colors/themes')['[data-theme=dark]'],
          primary: '#00a8dd',
          'primary-focus': '#004c85',
          secondary: '#004c85',
          error: '#ed1c24',
          'error-content': '#FFFFFF',
          neutral: '#fff',
          'base-100': '#333'
        }
      }
    ]
  },
  darkMode: 'class'
}
