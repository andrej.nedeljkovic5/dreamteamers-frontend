import axios from './apiCommon'
import { Request } from 'store/requestsSlice'

export const getRequests = async (
  status?: string,
  all?: boolean,
  startDate?: Date | null,
  endDate?: Date | null,
  period?: string,
  employee?: string
): Promise<Request[]> => {
  return await axios
    .get<Request[]>('requests', { params: { status, all, start_date: startDate, end_date: endDate, period, employee } })
    .then((response) => {
      return response.map((req) => {
        return { ...req, start_date: new Date(req.start_date), end_date: new Date(req.end_date) }
      })
    })
}

export default {
  getRequests
}
