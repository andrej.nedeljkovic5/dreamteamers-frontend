import { UserData } from 'store/authSlice'
import { EmployeeUser } from 'store/employeesSlice'
import { Role } from 'store/roleSlice'
import axios from './apiCommon'

export const getEmployees = async (teamId?: number): Promise<EmployeeUser[]> => {
  return await axios.get<EmployeeUser[]>('users', { params: { group_id: teamId } }).then((response) => {
    return response
  })
}

export const getAdditionalData = async (): Promise<UserData> => {
  return await axios.get<UserData>('users/home')
}

export const getRoles = async (): Promise<Role[]> => {
  // return await axios.get<Role[]>('roles').then((response) => {
  //   return response
  // })

  // Mocked data
  return await new Promise((res, rej) => {
    setTimeout(() => {
      res([
        {
          id: 1,
          name: 'admin'
        },
        {
          id: 3,
          name: 'employee'
        }
      ])
    }, 1500)
  })
}

export const saveUser = async (
  name: string,
  email: string,
  userRole: string,
  position: string,
  employedSince: Date | null,
  freeDays: string,
  team: string[]
): Promise<number> => {
  return await axios.post<number>('users/save-user', {
    name,
    email,
    role_id: userRole,
    job_title: position,
    employed_since: employedSince,
    days_left: parseInt(freeDays),
    teams: team
  })
}

export default {
  getEmployees,
  getAdditionalData,
  getRoles,
  saveUser
}
