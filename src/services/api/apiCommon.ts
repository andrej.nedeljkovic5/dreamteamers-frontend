import axios, { AxiosError, AxiosResponse } from 'axios'

const REQUEST_TIMEOUT_MS = 2500

export class ApiError extends Error {
  public statusCode: number
  public error: string
  public data: any

  constructor(message: string, statusCode: number, data?: any) {
    super(message)
    this.name = this.constructor.name
    this.statusCode = statusCode
    this.error = message
    this.data = data
  }
}

export interface ValidationMessage {
  type: string
  message: string
}

export class ValidationError extends Error {
  public statusCode: number
  public messages: ValidationMessage[]

  constructor(
    messages: Array<{
      type: string
      message: string
    }>,
    statusCode: number
  ) {
    super()
    this.name = this.constructor.name
    this.statusCode = statusCode
    this.messages = messages
  }
}

const axiosInstance = axios.create({
  baseURL: 'http://localhost:3001/',
  headers: {
    'Content-type': 'application/json'
  },
  timeout: REQUEST_TIMEOUT_MS
})

export const setAccessToken = (token?: string): void => {
  axiosInstance.defaults.headers.common.Authorization = token ? `Bearer ${token}` : false
}

_addResponseInterceptors()

function _addResponseInterceptors(): void {
  axiosInstance.interceptors.response.use(handleResponse, handleErrorResponse)

  async function handleErrorResponse(res: AxiosError<any>): Promise<ApiError> {
    console.log('::::::::: API RESPONSE ERROR ::::::::::\n', {
      endpoint: `${res.config?.method ?? ''}' '${res.config?.url ?? ''}`,
      request: res.config?.data,
      messages: res.response?.data?.message
    })

    const message = res?.response?.data?.message
    const status = res?.response?.status

    // todo: show error toast

    if (status === 400) {
      const errors: Array<{
        constraints?: {
          [type: string]: string
        }
        property: string
        value: string
      }> = message

      const messages: ValidationMessage[] = []

      errors.forEach((err) => {
        for (const constraint in err.constraints) {
          messages.push({
            type: err.property,
            message: err.constraints[constraint]
          })
        }
      })

      throw new ValidationError(messages, 400)
    } else if (status === 401) {
      throw new ApiError('Wrong username or password', status)
    } else if (status === 403) {
      throw new ApiError('Request not authorized.', status)
    } else if (status === 404) {
      throw new ApiError('Not found', status)
    } else if (status === 409) {
      throw new ApiError('Conflict.', status)
    }

    throw new ApiError(message, 500)
  }

  function handleResponse(res: AxiosResponse): Promise<any> | any {
    printResponse(res)

    // If session has changed until response arrived (logout happened)
    if (
      res?.config?.headers?.Authorization &&
      axiosInstance.defaults?.headers?.common?.Authorization &&
      res?.config?.headers?.Authorization !== axiosInstance.defaults?.headers?.common?.Authorization
    ) {
      console.log('Ignoring the response for obsolete session.', {
        oldAuthorization: res?.config?.headers?.Authorization,
        newAuthorization: axiosInstance.defaults?.headers?.common?.Authorization
      })
      return Promise.reject(new ApiError('Obsolete data.', 500))
    }

    const { data, status } = res
    console.log('**** data, status: ', data, status)
    if (is2xx(status)) {
      return data
    } else {
      // errorToast('Unexpected error occurred. Please contact support.')
      return Promise.reject(new ApiError(data.message, status))
    }
  }

  function printResponse(res: AxiosResponse): void {
    console.log(':::::::::: API RESPONSE ::::::::::\n', {
      status: res.status,
      endpoint: `${res.config?.method ?? ''} ${res.config?.url ?? ''}`,
      response: res.data
    })
  }

  function is2xx(code: number): boolean {
    return code >= 200 && code < 300
  }
}

export default axiosInstance
