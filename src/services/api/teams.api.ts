import axios from './apiCommon'
import { Team } from 'store/teamsSlice'

export const getTeams = async (details?: boolean): Promise<Team[]> => {
  return await axios.get<Team[]>('groups', { params: { details } }).then((response) => {
    return response
  })
}

export default {
  getTeams
}
