import authApi from './auth.api'
import usersApi from './users.api'
import teamsApi from './teams.api'
import requestsApi from 'services/api/requests.api'

const api = {
  auth: authApi,
  users: usersApi,
  teams: teamsApi,
  requests: requestsApi
}

export default api
