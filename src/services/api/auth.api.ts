import { AuthUser } from 'store/authSlice'
import axios from './apiCommon'

const login = async (email: string, password: string): Promise<AuthUser> => {
  return await axios
    .post<AuthUser>('auth/login', {
      email,
      password
    })
    .then((response: any) => {
      console.log('response: ', response)
      return response
    })
}

const forgotPassword = async (email: string): Promise<{ tokenGenerated: boolean; emailSent: boolean }> => {
  return await axios
    .post<{ tokenGenerated: boolean; emailSent: boolean }>('auth/reset-password', {
      email
    })
    .then((response: any) => {
      console.log('response: ', response)
      return response
    })
}

const resetPassword = async (password: string, email: string, token: string): Promise<{ passwordChanged: boolean }> => {
  return await axios
    .put<{ passwordChanged: boolean }>('auth/reset-password', {
      password,
      email,
      reset_password_token: token
    })
    .then((response: any) => {
      console.log('response: ', response)
      return response
    })
}

export default {
  login,
  forgotPassword,
  resetPassword
}
