import React from 'react'
import { createBrowserRouter, createRoutesFromElements, Route, RouterProvider } from 'react-router-dom'

import { Fallback, todosAction, todosLoader, TodosList, TodosBoundary, todoLoader, Todo } from 'routing/routes'
import { Layout } from 'routing/Layout'
import RequireRoles from 'routing/RequireRoles'

import './App.css'
import { GetAuth, RedirectIfAuth, RequireAuth } from 'routing/Auth'
import { AuthLayout } from 'routing/AuthLayout'
import Login from './pages/Login'
import { BasicLayout } from 'routing/BasicLayout'
import Employees from 'pages/Employees/Employees'
import ForgotPassword from './pages/ForgotPassword'
import ResetPassword from './pages/ResetPassword'
import Requests from 'pages/Requests'
import CreatePassword from 'pages/CreatePassword/CreatePassword'
import AllTeams from 'pages/AllTeams'
import NotExistPage from 'components/NotExistPage'
import CalendarPage from 'pages/Calendar/CalendarPage'
import Dashboard from 'pages/Dashboard'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<BasicLayout />}>
      <Route element={<GetAuth />}>
        <Route element={<Layout />}>
          <Route element={<RequireAuth />}>
            <Route path="/" element={<Dashboard />} />
            <Route path="calendar" element={<CalendarPage />} />
            <Route path="requests" element={<Requests />} />
            <Route
              path="todos"
              action={todosAction}
              loader={todosLoader}
              element={<TodosList />}
              errorElement={<TodosBoundary />}
            >
              <Route path=":id" loader={todoLoader} element={<Todo />} />
            </Route>
            <Route element={<RequireRoles roles={['admin', 'group_admin']} />}>
              <Route path="employees" element={<Employees />} />
              <Route path="teams" element={<AllTeams />} />
            </Route>
          </Route>
        </Route>
        <Route path="/auth" element={<AuthLayout />}>
          <Route element={<RedirectIfAuth />}>
            <Route path="login" element={<Login />} />
            <Route path="reset-password" element={<ResetPassword />} />
            <Route path="forgot-password" element={<ForgotPassword />} />
            <Route path="create-password" element={<CreatePassword />} />
          </Route>
        </Route>
      </Route>
      <Route path="*" element={<NotExistPage />} />
    </Route>
  )
)

function App(): React.ReactElement {
  return <RouterProvider router={router} fallbackElement={<Fallback />} />
}

export default App
