import White from 'assets/assets/ovals_stars/Oval 1.png'
import Yellow from 'assets/assets/ovals_stars/Oval 2.png'
import Orange from 'assets/assets/ovals_stars/Oval 3.png'
import Red from 'assets/assets/ovals_stars/Oval 4.png'
import Pink from 'assets/assets/ovals_stars/Oval 5.png'
import Lilac from 'assets/assets/ovals_stars/Oval 6.png'
import Purple from 'assets/assets/ovals_stars/Oval 7.png'
import Cyan from 'assets/assets/ovals_stars/Oval 8.png'
import LightBlue from 'assets/assets/ovals_stars/Oval 9.png'
import Blue from 'assets/assets/ovals_stars/Oval 10.png'
import DarkBlue from 'assets/assets/ovals_stars/Oval 11.png'
import LightGreen from 'assets/assets/ovals_stars/Oval 12.png'
import NeonGreen from 'assets/assets/ovals_stars/Oval 13.png'
import Green from 'assets/assets/ovals_stars/Oval 14.png'
import DarkGreen from 'assets/assets/ovals_stars/Oval 15.png'
import Brown from 'assets/assets/ovals_stars/Oval 16.png'
import Gray from 'assets/assets/ovals_stars/Oval 17.png'
import Black from 'assets/assets/ovals_stars/Oval 18.png'

export function getOval(color: string | undefined): string {
  if (color) {
    return ovals.get(color) || ovals.get('white')
  } else {
    return ovals.get('white')
  }
}
export const ovals: ReadonlyMap<string, any> = new Map([
  ['white', White],
  ['yellow', Yellow],
  ['orange', Orange],
  ['red', Red],
  ['pink', Pink],
  ['lilac', Lilac],
  ['purple', Purple],
  ['cyan', Cyan],
  ['light blue', LightBlue],
  ['blue', Blue],
  ['dark blue', DarkBlue],
  ['light green', LightGreen],
  ['neon green', NeonGreen],
  ['green', Green],
  ['dark green', DarkGreen],
  ['brown', Brown],
  ['gray', Gray],
  ['black', Black]
])
