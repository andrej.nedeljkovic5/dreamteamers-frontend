import React from 'react'
import type { ActionFunctionArgs, LoaderFunctionArgs } from 'react-router-dom'
import {
  Form,
  Link,
  Outlet,
  useFetcher,
  useLoaderData,
  useNavigation,
  useParams,
  useRouteError
} from 'react-router-dom'

import type { Todos } from './todos'
import { addTodo, deleteTodo, getTodos } from './todos'

export async function sleep(n: number = 500): Promise<void> {
  return await new Promise((r) => setTimeout(r, n))
}

export const Fallback: React.FC = () => {
  return <p>Performing initial data &quot;load&quot;</p>
}

// Todos
export async function todosAction({ request }: ActionFunctionArgs): Promise<Response | { ok: true }> {
  await sleep()

  const formData = await request.formData()

  // Deletion via fetcher
  if (formData.get('action') === 'delete') {
    const id = formData.get('todoId')
    if (typeof id === 'string') {
      deleteTodo(id)
      return { ok: true }
    }
  }

  // Addition via <Form>
  const todo = formData.get('todo')
  if (typeof todo === 'string') {
    addTodo(todo)
  }

  return new Response(null, {
    status: 302,
    headers: { Location: '/todos' }
  })
}

export async function todosLoader(): Promise<Todos> {
  await sleep()
  return getTodos()
}

export const TodosList: React.FC = () => {
  const todos = useLoaderData() as Todos
  const navigation = useNavigation()
  const formRef = React.useRef<HTMLFormElement>(null)

  // If we add and then we delete - this will keep isAdding=true until the
  // fetcher completes its revalidation
  const [isAdding, setIsAdding] = React.useState(false)
  React.useEffect(() => {
    if (navigation.formData?.get('action') === 'add') {
      setIsAdding(true)
    } else if (navigation.state === 'idle') {
      setIsAdding(false)
      formRef.current?.reset()
    }
  }, [navigation])

  return (
    <>
      <h2>Todos</h2>
      <p>
        This todo app uses a &lt;Form&gt; to submit new todos and a &lt;fetcher.form&gt; to delete todos. Click on a
        todo item to navigate to the /todos/:id route.
      </p>
      <ul>
        <li>
          <Link to="/todos/junk">Click this link to force an error in the loader</Link>
        </li>
        {Object.entries(todos).map(([id, todo]) => (
          <li key={id}>
            <TodoItem id={id} todo={todo} />
          </li>
        ))}
      </ul>
      <Form method="post" ref={formRef}>
        <input type="hidden" name="action" value="add" />
        <input name="todo" />
        <button type="submit" disabled={isAdding}>
          {isAdding ? 'Adding...' : 'Add'}
        </button>
      </Form>
      <Outlet />
    </>
  )
}

export const TodosBoundary: React.FC = () => {
  const error = useRouteError() as Error
  return (
    <>
      <h2>Error 💥</h2>
      <p>{error.message}</p>
    </>
  )
}

interface TodoItemProps {
  id: string
  todo: string
}

export const TodoItem: React.FC<TodoItemProps> = ({ id, todo }) => {
  const fetcher = useFetcher()

  const isDeleting = fetcher.formData != null
  return (
    <>
      <Link to={`/todos/${id}`}>{todo}</Link>
      &nbsp;
      <fetcher.Form method="post" style={{ display: 'inline' }}>
        <input type="hidden" name="action" value="delete" />
        <button type="submit" name="todoId" value={id} disabled={isDeleting}>
          {isDeleting ? 'Deleting...' : 'Delete'}
        </button>
      </fetcher.Form>
    </>
  )
}

// Todo
export async function todoLoader({ params }: LoaderFunctionArgs): Promise<string> {
  await sleep()
  const todos = getTodos()
  if (!params.id) {
    throw new Error('Expected params.id')
  }
  const todo = todos[params.id]
  if (!todo) {
    throw new Error(`Uh oh, I couldn't find a todo with id "${params.id}"`)
  }
  return todo
}

export const Todo: React.FC = () => {
  const params = useParams()
  const todo = useLoaderData() as string
  return (
    <>
      <h2>Nested Todo Route:</h2>
      <p>id: {params.id}</p>
      <p>todo: {todo}</p>
    </>
  )
}
