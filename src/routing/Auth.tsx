import { Navigate, Outlet } from 'react-router-dom'
import React, { useEffect } from 'react'

import { useAppDispatch, useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import { AuthUser, initializeApp } from 'store/authSlice'

import whiteLogo from 'assets/logo_favicon/DreamTeamers logo big-04.png'

export const GetAuth = (): React.ReactElement => {
  const dispatch = useAppDispatch()
  const appInitializedStatus = useAppSelector((state: RootState): boolean => state.auth.appInitializedStatus)

  useEffect(() => {
    dispatch(initializeApp())
  }, [])

  return appInitializedStatus ? (
    <Outlet />
  ) : (
    <div className="flex min-h-screen w-full items-center justify-center bg-blue">
      <img src={whiteLogo} alt="logo" className="w-2/4" />
    </div>
  )
}

export const RequireAuth = (): React.ReactElement => {
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)

  if (!user) {
    return <Navigate to={{ pathname: '/auth/login' }} replace />
  }

  return <Outlet />
}

export const RedirectIfAuth = (): React.ReactElement => {
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)

  if (user) {
    return <Navigate to={{ pathname: '/' }} replace />
  }

  return <Outlet />
}
