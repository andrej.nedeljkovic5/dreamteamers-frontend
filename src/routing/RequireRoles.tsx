import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { RootState } from 'store/store'
import { useAppSelector } from 'store/hooks'
import { AuthUser } from 'store/authSlice'

interface RequireRolesProps {
  roles: Array<'employee' | 'group_admin' | 'admin'>
}

const RequireRoles = ({ roles }: RequireRolesProps): React.ReactElement => {
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)
  if (user?.roles.some((role) => roles.includes(role))) {
    return <Outlet />
  }
  return <Navigate to={{ pathname: '/' }} replace />
}

export default RequireRoles
