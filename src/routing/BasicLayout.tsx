import React, { useEffect } from 'react'
import { Outlet } from 'react-router-dom'
import { themeChange } from 'theme-change'

export const setThemeInHTML = (): 'dark' | 'light' => {
  // On page load or when changing themes, best to add inline in `head` to avoid FOUC
  if (
    localStorage.theme === 'dark' ||
    (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)
  ) {
    document.documentElement.classList.add('dark')
    localStorage.theme = 'dark'
    return 'dark'
  } else {
    document.documentElement.classList.remove('dark')
    localStorage.theme = 'light'
    return 'light'
  }
}

export const BasicLayout: React.FC = () => {
  useEffect(() => {
    setThemeInHTML()
  }, [])

  useEffect(() => {
    themeChange(false)
  }, [localStorage.theme])

  return (
    <div
      className="
        flex min-h-[100vh] flex-col items-center
    "
    >
      <Outlet />
    </div>
  )
}
