import React, { useEffect, useMemo, useState, createContext } from 'react'
import { Link, Outlet, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { RootState } from 'store/store'
import { AuthUser, logout } from 'store/authSlice'
import { getNotifications } from 'store/notifcationsSlice'
import { useAppDispatch } from 'store/hooks'
import routes from 'routing/Navigation/routes'
import Navigation from './Navigation/Navigation'
import Notifications from 'components/Notifications/Notifications'

export const ThemeContext = createContext<any>({})

export const Layout: React.FC = () => {
  const [myThemeState, setMyThemeState] = useState<string | null>(localStorage.getItem('theme'))
  const notifications = useSelector((state: RootState) => state.notifications.notifications)
  const [notificationsOpen, setNotificationsOpen] = useState(false)
  const [sideBarOpen, setSideBarOpen] = useState(false)
  const user = useSelector((state: RootState): AuthUser | null => state.auth.user)
  const userName = user?.name
  const userRoles = user?.roles
  const dispatch = useAppDispatch()

  useEffect(() => {
    void dispatch(getNotifications())
  }, [])

  const location = useLocation().pathname.slice(1)

  const toggleSideBar = (): void => {
    setSideBarOpen((current) => !current)
  }

  const handleLogout = (): void => {
    setSideBarOpen((current) => !current)
    dispatch(logout())
  }

  const route = useMemo(
    () => (
      <>
        {routes
          .filter((val) => userRoles?.some((role: any) => val.scopes.includes(role)))
          .map((val) => {
            return (
              <li
                className="pointer-cursor relative pt-8 hover:text-blue dark:text-white dark:hover:text-blue"
                key={val.label}
              >
                <Link onClick={toggleSideBar} to={val.path}>
                  {val.label}
                </Link>
              </li>
            )
          })}
        <li className="pointer-cursor relative pt-8 hover:text-blue dark:text-white dark:hover:text-blue">
          <Link onClick={handleLogout} to="/auth/login">
            Log out
          </Link>
        </li>
      </>
    ),
    []
  )

  const toggleNotifications = (): void => {
    setNotificationsOpen((current) => !current)
  }
  return (
    <ThemeContext.Provider value={{ myThemeState, setMyThemeState }}>
      <Navigation
        sideBarOpen={sideBarOpen}
        userName={userName}
        location={location}
        routes={route}
        toggleSideBar={toggleSideBar}
        toggleNotifications={toggleNotifications}
        notificationLength={notifications.length}
      />
      <Notifications
        toggleNotifications={toggleNotifications}
        notificationOpen={notificationsOpen}
        notifications={notifications}
      />
      <Outlet />
    </ThemeContext.Provider>
  )
}
