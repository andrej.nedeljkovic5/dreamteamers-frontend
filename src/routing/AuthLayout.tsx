import React from 'react'
import { Outlet } from 'react-router-dom'
import coloredLogo from 'assets/logo_favicon/DreamTeamers logo big-06.png'
import whiteLogo from 'assets/logo_favicon/DreamTeamers logo big-04.png'

export const AuthLayout: React.FC = () => {
  return (
    <main className="flex w-full grow-[1] items-center justify-center bg-blue dark:bg-blue-dark">
      <div className="flex w-[300px] flex-col gap-7 rounded-sm bg-white p-30 dark:bg-font-main">
        <img
          src={
            localStorage.theme === 'light' || window.matchMedia('(prefers-color-scheme: light)').matches
              ? coloredLogo
              : whiteLogo
          }
          alt="logo"
          className="h-11 object-contain"
        />
        <Outlet />
      </div>
    </main>
  )
}
