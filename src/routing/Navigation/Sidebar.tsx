import React, { ReactElement } from 'react'
import cx from 'classnames'
import { ReactComponent as Close } from 'assets/icons/close.svg'
import coloredLogo from 'assets/logo_favicon/DreamTeamers logo big-06.png'
import whiteLogo from 'assets/logo_favicon/DreamTeamers logo big-04.png'

interface BarProps {
  sideBar: boolean
  exitSideBar: () => void
  routes: JSX.Element
}

const Sidebar = (props: BarProps): ReactElement => {
  return (
    <>
      <div
        className={cx(
          'easy-in duration-400 absolute z-50 h-full w-[80%] -translate-x-full bg-white pl-10 pt-2 text-black drop-shadow-md transition duration-200 dark:bg-font-main sm:w-80',
          { 'translate-x-0': props.sideBar }
        )}
      >
        <div className="flex">
          <img
            className="w-21 relative -left-1 top-1.5 h-11"
            src={
              localStorage.theme === 'light' || window.matchMedia('(prefers-color-scheme: light)').matches
                ? coloredLogo
                : whiteLogo
            }
            alt="Logo"
          />
          <Close
            className="absolute right-3.5 top-3.5 h-5 w-5 cursor-pointer dark:invert"
            onClick={props.exitSideBar}
          />
        </div>
        <ul className="cursor-pointer list-none flex-col">{props.routes}</ul>
      </div>
      {props.sideBar ? (
        <div onClick={props.exitSideBar} className="absolute z-40 h-full w-full bg-black opacity-20"></div>
      ) : (
        <div />
      )}
    </>
  )
}

export default Sidebar
