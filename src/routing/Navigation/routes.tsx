interface RouteItem {
  path: string
  label: string
  scopes: Array<'admin' | 'group_admin' | 'employee'>
}

const routes: RouteItem[] = [
  {
    label: 'Dashboard',
    path: '/',
    scopes: ['admin', 'group_admin', 'employee']
  },
  {
    label: 'Calendar',
    path: '/calendar',
    scopes: ['admin', 'group_admin', 'employee']
  },
  {
    label: 'Employees',
    path: '/employees',
    scopes: ['admin', 'group_admin']
  },
  {
    label: 'Requests',
    path: '/requests',
    scopes: ['admin', 'group_admin', 'employee']
  },
  {
    label: 'Teams',
    path: '/teams',
    scopes: ['admin', 'group_admin']
  }
]

export default routes
