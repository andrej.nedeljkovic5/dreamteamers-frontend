import React, { useState, useEffect, useMemo, useContext } from 'react'
import { useMedia } from 'react-use'
import { ThemeContext } from 'routing/Layout'
import { Navbar } from 'react-daisyui'
import Sidebar from 'routing/Navigation/Sidebar'
import { themeChange } from 'theme-change'
import { setThemeInHTML } from 'routing/BasicLayout'
import { ReactComponent as Menu } from 'assets/icons/menu.svg'
import { ReactComponent as Notification } from 'assets/icons/notifications.svg'
import coloredLogo from 'assets/logo_favicon/DreamTeamers logo big-06.png'
import whiteLogo from 'assets/logo_favicon/DreamTeamers logo big-04.png'
import smallLogo from 'assets/logo_favicon/DreamTeamers logo big-05.png'

interface NavProps {
  sideBarOpen: boolean
  userName: string | undefined
  location: string
  routes: JSX.Element
  toggleSideBar: () => void
  toggleNotifications: () => void
  notificationLength: number
}

const Navigation: React.FC<NavProps> = (props: NavProps) => {
  const { myThemeState, setMyThemeState } = useContext(ThemeContext)
  const [theme, setTheme] = useState(setThemeInHTML())
  const isSmallScreen = useMedia('(max-width: 768px)')
  const handleDarkThemeToggle = (): void => {
    if (theme === 'light') {
      localStorage.theme = 'dark'
      setTheme('dark')
      setMyThemeState('dark')
    } else {
      localStorage.theme = 'light'
      setTheme('light')
      setMyThemeState('light')
    }
  }

  useEffect(() => {
    themeChange(false)
  }, [])

  const logo = useMemo(() => {
    return isSmallScreen
      ? smallLogo
      : localStorage.theme === 'light' || window.matchMedia('(prefers-color-scheme: light)').matches
      ? coloredLogo
      : whiteLogo
  }, [isSmallScreen, theme])

  return (
    <div className="z-40 w-full">
      <Sidebar sideBar={props.sideBarOpen} exitSideBar={props.toggleSideBar} routes={props.routes} />
      <Navbar className="m-auto flex h-15 w-full items-center justify-between bg-white px-2 drop-shadow-md dark:bg-font-main md:px-4">
        <div className="flex h-full items-center gap-1 md:w-[300px] md:gap-3 lg:w-[370px]">
          <Menu onClick={props.toggleSideBar} className="h-8 w-8 cursor-pointer dark:invert" />
          <img className="h-8 md:h-11" src={logo} alt="dreamteamers logo" />
          <label htmlFor="default-toggle" className="relative inline-flex cursor-pointer items-center">
            <input
              type="checkbox"
              checked={theme === 'dark'}
              onChange={handleDarkThemeToggle}
              id="default-toggle"
              className="peer sr-only"
              data-toggle-theme="dark,light"
              data-act-class="ACTIVECLASS"
            />
            <div className="peer-checked:bg-blue-600 peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-[2px] after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:outline-none dark:border-gray-600 dark:bg-gray-700" />
            <span className="ml-1 text-sm font-medium md:ml-3">{theme === 'light' ? 'Light theme' : 'Dark theme'}</span>
          </label>
        </div>
        <div className="mr-auto ml-4 capitalize text-font-main dark:text-white md:mr-0 md:ml-0">{props.location}</div>
        <div className="flex items-center justify-end gap-3 md:w-[300px] lg:w-[370px]">
          <div className="relative">
            <Notification onClick={props.toggleNotifications} className="h-10 w-10 cursor-pointer dark:invert" />
            {props.notificationLength > 0 && (
              <div className="absolute top-0 right-1 z-10 flex h-3.5 w-3.5 items-center justify-center rounded-full bg-blue">
                <span className="text-[11px] text-white">{props.notificationLength}</span>
              </div>
            )}
          </div>
          <div className="hidden h-9 w-9 rounded-full bg-blue md:block" />
          <p className="hidden text-font-main dark:text-white md:block">{props.userName}</p>
        </div>
      </Navbar>
    </div>
  )
}

export default Navigation
