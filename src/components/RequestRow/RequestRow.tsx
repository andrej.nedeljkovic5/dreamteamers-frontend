import React, { useState } from 'react'
import { Request } from 'store/requestsSlice'
import { AuthUser } from 'store/authSlice'
import { useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import VacationRequestDialog from 'components/VacationRequestDialog'
import approved_request from 'assets/assets/approved_request.png'
import pending_request from 'assets/assets/pending_request.png'
import declined_request from 'assets/assets/declined_request.png'
import right_skinny_arrow from 'assets/icons/right_skinny_arrow.svg'

interface RequestProps {
  request: Request
  simple?: boolean
}

const RequestRow: React.FC<RequestProps> = ({ request, simple }) => {
  const [dialogOpen, setDialogOpen] = useState(false)
  const toggleDialogOpen = (): void => {
    setDialogOpen(!dialogOpen)
  }

  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)

  const formattedStartDate = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'long',
    day: '2-digit'
  }).format(request.start_date)
  const formattedEndDate = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'long',
    day: '2-digit'
  }).format(request.end_date)
  const handleClick = (): void => {
    setDialogOpen((prev) => !prev)
  }
  return (
    <tr className="divide-x border">
      {user?.roles.some((role) => role === 'group_admin' || role === 'admin') && !simple && (
        <td className="w-1/6">
          <div className="flex gap-1">{request.name}</div>
        </td>
      )}
      <td className="relative w-1/6">{formattedStartDate + ' - ' + formattedEndDate}</td>
      {!simple && <td className="relative w-1/6">{request.label}</td>}
      <td className="relative w-1/6">{request.duration} days</td>
      {!simple && <td className="relative w-1/6">{request.days_left} days</td>}
      <td className="relative w-1/6">
        <div className="flex items-center justify-between align-top">
          {request.status === 'approved' ? (
            <img width="30" height="30" src={approved_request} />
          ) : request.status === 'pending' || request.urgent ? (
            <img width="30" height="30" src={pending_request} />
          ) : (
            <img width="30" height="30" src={declined_request} />
          )}
          {request.status}
          <img
            className="cursor-pointer dark:invert"
            width="20"
            height="20"
            src={right_skinny_arrow}
            onClick={() => handleClick()}
          ></img>
        </div>
      </td>
      {dialogOpen && (
        <VacationRequestDialog dialogOpen={dialogOpen} toggleDialogOpen={toggleDialogOpen} request={request} />
      )}
    </tr>
  )
}

export default RequestRow
