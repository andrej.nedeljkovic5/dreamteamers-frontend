import React, { useState } from 'react'

import { useAppSelector, useAppDispatch } from 'store/hooks'
import { decrement, increment, incrementByAmount, incrementAsync, incrementIfOdd } from 'store/counterSlice'
import { RootState } from 'store/store'
import Button from 'components/Button'

const Counter: React.FC = () => {
  const count = useAppSelector((state: RootState): number => state.counter.value)
  const dispatch = useAppDispatch()
  const [incrementAmount, setIncrementAmount] = useState('2')

  const incrementValue = +(Number(incrementAmount) !== 0 || 0)

  return (
    <div>
      <div className="flex items-center justify-center">
        <Button aria-label="Decrement value" onClick={() => dispatch(decrement())}>
          -
        </Button>
        <span className="mx-4 text-7xl">{count}</span>
        <Button aria-label="Increment value" onClick={() => dispatch(increment())}>
          +
        </Button>
      </div>
      <div className="flex items-center justify-center">
        <input
          className="w-16 rounded bg-amber-100 p-0.5 text-center text-3xl text-black"
          aria-label="Set increment amount"
          value={incrementAmount}
          onChange={(e) => setIncrementAmount(e.target.value)}
        />
        <Button onClick={() => dispatch(incrementByAmount(incrementValue))}>Add Amount</Button>
        <Button
          // wrap async actions with { void .. } to avoid TS error for ignoring promises
          onClick={() => {
            void dispatch(incrementAsync())
          }}
        >
          Add Async
        </Button>
        <Button onClick={() => dispatch(incrementIfOdd(incrementValue))}>Add If Odd</Button>
      </div>
    </div>
  )
}

export default Counter
