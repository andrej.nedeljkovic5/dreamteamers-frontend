import React from 'react'
import { Team } from 'store/teamsSlice'
import { ReactComponent as Edit } from 'assets/icons/edit.svg'
import { ReactComponent as Delete } from 'assets/icons/delete.svg'
import { ReactComponent as Arrow } from 'assets/icons/right_skinny_arrow.svg'
import { getOval } from 'utils/ovals'
import { useNavigate } from 'react-router-dom'
interface TeamProps {
  team: Team
}
const TeamRow: React.FC<TeamProps> = ({ team }) => {
  const navigate = useNavigate()

  return (
    <tr className="divide-x border ">
      <td className="w-2/5 ">
        <div className="flex gap-1">
          <img src={getOval(team.color)} alt={`(${team.color ? team.color : 'undefined color'}) `} className="w-6" />
          {team.name}
        </div>
      </td>
      <td className="w-1/5">
        {team?.admins?.map((admin) => {
          return <div key={admin}>{admin}</div>
        })}
      </td>
      <td>{team?.employeesCount}</td>
      <td
        className="relative w-1/6"
        onClick={() => {
          console.log('Arrow')
          navigate(`/teams/team-details/${team.id}`)
        }}
      >
        <div className="hover:cursor-pointer">Team Details</div>
        <Arrow className="absolute right-0 top-1/4 w-5 hover:cursor-pointer dark:invert" />
      </td>
      <td>
        <Edit
          className="inline w-6 hover:cursor-pointer dark:invert"
          onClick={() => {
            console.log('Edit')
          }}
        />
        <Delete
          className="inline w-6 hover:cursor-pointer dark:invert"
          onClick={() => {
            console.log('Delete')
          }}
        />
      </td>
    </tr>
  )
}

export default TeamRow
