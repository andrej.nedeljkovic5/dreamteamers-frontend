import React from 'react'
import { useNavigate } from 'react-router-dom'
import Button from 'components/Button'
import './NotExistPage.css'

const NotExistPage: React.FC = () => {
  const navigate = useNavigate()
  return (
    <div className="h-screen w-full content-center items-center justify-center bg-blue bg-cover">
      <div className="mx-auto text-center text-2xl text-white">404 NOT FOUND</div>
      <div className="mx-auto mt-10 text-center">
        <Button
          onClick={() => {
            navigate('/')
          }}
        >
          Back to main page
        </Button>
      </div>
      <div className="mt-24 flex justify-center">
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/xm3YgoEiEDc?autoplay=1&mute=1"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      </div>
    </div>
  )
}
export default NotExistPage
