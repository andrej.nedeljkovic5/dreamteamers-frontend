import React from 'react'
import cx from 'classnames'

const Select = React.forwardRef<HTMLSelectElement, React.HTMLAttributes<HTMLSelectElement>>(function Select(
  // Workaround for className prop. Eslint throws an error when React.HTMLAttributes<HTMLSelectElement> is used straight away.
  // This might be fixed in future eslint versions as this is not the expected behaviour.
  // eslint-disable-next-line react/prop-types
  { children, className, ...props },
  ref
) {
  return (
    <select
      ref={ref}
      className={cx(
        'h-8 w-full appearance-none border-b-2 border-medium-gray bg-transparent bg-[url(assets/icons/drop_down.svg)] bg-[length:15px_15px] bg-[right_4px_bottom_4px] bg-no-repeat px-1 text-sm text-font-main placeholder:text-font-main hover:cursor-pointer focus:outline-none dark:text-white dark:placeholder-white md:w-48',
        className
      )}
      {...props}
    >
      {children}
    </select>
  )
})

export default Select
