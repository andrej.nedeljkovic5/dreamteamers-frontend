import React, { ReactElement } from 'react'
import cx from 'classnames'
import ErrorText from 'components/ErrorText/ErrorText'

interface TextInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  placeholder: string
  name: string
  helperText?: string | false
  hideHelperText?: boolean
  error?: boolean
}

const TextInput: React.FC<TextInputProps> = ({
  placeholder,
  name,
  disabled,
  className,
  helperText,
  hideHelperText,
  error,
  ...rest
}): ReactElement => (
  <div className={className}>
    <div className={cx('relative z-0 flex h-10 flex-col justify-end', className)}>
      <input
        {...rest}
        id={name}
        className={cx(
          'border-gray peer block w-full appearance-none border-b-2 pt-2 pb-0.5 text-sm focus:outline-none focus:ring-0',
          {
            'bg-gray/20': disabled,
            'bg-transparent': !disabled,
            'border-red': error
          }
        )}
        placeholder=" "
        name={name}
        disabled={disabled}
      />
      <label
        htmlFor="input"
        className="absolute top-4 -z-10 origin-[0] -translate-y-6 scale-[0.85] transform text-sm text-font-main/50 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-[0.85] dark:text-white/50"
      >
        {placeholder}
      </label>
    </div>
    {!hideHelperText && <ErrorText>{helperText}</ErrorText>}
  </div>
)

export default TextInput
