import React from 'react'
import { Team } from 'store/teamsSlice'
import { ReactComponent as Edit } from 'assets/icons/edit.svg'
import { ReactComponent as Delete } from 'assets/icons/delete.svg'
import { ReactComponent as Arrow } from 'assets/icons/right_skinny_arrow.svg'
import { getOval } from 'utils/ovals'
import { useNavigate } from 'react-router-dom'

interface TeamProps {
  team: Team
}
const TeamCard: React.FC<TeamProps> = ({ team }) => {
  const navigate = useNavigate()
  return (
    <div className="relative mr-60 w-full pb-2">
      <table className="w-11/12">
        <tbody>
          <tr>
            <td className="w-1/3 py-2">Team Name: </td>
            <td>
              <div className="flex gap-1">
                <img
                  src={getOval(team.color)}
                  alt={`(${team.color ? team.color : 'undefined color'}) `}
                  className="w-6"
                />
                {team.name}
              </div>
            </td>
          </tr>
          <tr>
            <td className="w-1/3  py-2">Team Admin: </td>
            <td>
              {team?.admins?.map((admin) => {
                return <div key={admin}>{admin}</div>
              })}
            </td>
          </tr>
          <tr>
            <td className="w-1/3  py-2">Team Members: </td>
            <td>{team?.employeesCount}</td>
          </tr>
        </tbody>
      </table>
      <Arrow
        className="w-1/16 absolute bottom-1/2 right-0 w-8 hover:cursor-pointer dark:invert"
        onClick={() => {
          console.log('Arrow')
          navigate(`/teams/team-details/${team.id}`)
        }}
      />
      <div className="flex justify-end gap-4">
        <Edit
          className="inline w-8 hover:cursor-pointer dark:invert"
          onClick={() => {
            console.log('Edit')
          }}
        />
        <Delete
          className="inline w-8 hover:cursor-pointer dark:invert"
          onClick={() => {
            console.log('Delete')
          }}
        />
      </div>
    </div>
  )
}

export default TeamCard
