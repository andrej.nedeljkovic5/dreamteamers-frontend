import React, { ReactElement } from 'react'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

const LoadingTeamCard: React.FC = (): ReactElement => (
  <div className="relative w-screen">
    <table className="w-11/12">
      <tbody>
        <tr className="w-full">
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
        </tr>
        <tr className="w-full">
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
        </tr>
        <tr className="w-full">
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
)

export default LoadingTeamCard
