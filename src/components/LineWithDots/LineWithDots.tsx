import React from 'react'
import cx from 'classnames'

interface LineWithDotsProps {
  className?: string
}

const LineWithDots: React.FC<LineWithDotsProps> = ({ className }) => (
  <div
    className={cx(
      'h-[2px] bg-light-gray ',
      "before:float-left before:-mt-[2px] before:h-[6px] before:w-[6px] before:rounded-full before:bg-medium-gray before:content-['_']",
      "after:float-right after:-mt-[2px] after:h-[6px] after:w-[6px] after:rounded-full after:bg-medium-gray after:content-['_']",
      'before:dark:bg-white after:dark:bg-white',
      className
    )}
  />
)

export default LineWithDots
