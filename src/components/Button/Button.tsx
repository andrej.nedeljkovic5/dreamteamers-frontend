import React, { ReactElement } from 'react'
import cx from 'classnames'

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  type?: 'button' | 'submit' | 'reset'
  variant?: 'regular' | 'transparent'
}

const Button: React.FC<ButtonProps> = ({
  onClick,
  type,
  variant,
  disabled,
  className,
  children,
  ...rest
}): ReactElement => (
  <button
    onClick={onClick}
    type={type}
    className={cx(
      'btn btn-sm rounded-sm text-sm font-semibold leading-5',
      {
        'btn-primary dark:btn-secondary': !variant || variant === 'regular',
        'btn-ghost': variant === 'transparent',
        'btn-disabled bg-medium-gray text-very-light-gray': disabled
      },
      className
    )}
    {...rest}
  >
    {children}
  </button>
)

export default Button
