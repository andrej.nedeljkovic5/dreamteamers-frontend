import React, { useState } from 'react'
import { Request } from 'store/requestsSlice'
import VacationRequestDialog from 'components/VacationRequestDialog'
import { useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import { AuthUser } from 'store/authSlice'
import approved_request from 'assets/assets/approved_request.png'
import right_skinny_arrow from 'assets/icons/right_skinny_arrow.svg'
import pending_request from 'assets/assets/pending_request.png'
import declined_request from 'assets/assets/declined_request.png'

interface RequestProps {
  request: Request
}
const RequestCard: React.FC<RequestProps> = ({ request }) => {
  const [dialogOpen, setDialogOpen] = useState(false)

  const toggleDialogOpen = (): void => {
    setDialogOpen(!dialogOpen)
  }
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)

  const formattedStartDate = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'short',
    day: '2-digit'
  }).format(request.start_date)
  const formattedEndDate = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: 'short',
    day: '2-digit'
  }).format(request.end_date)

  const handleClick = (): void => {
    setDialogOpen((prev) => !prev)
  }
  return (
    <>
      {dialogOpen && (
        <VacationRequestDialog dialogOpen={dialogOpen} toggleDialogOpen={toggleDialogOpen} request={request} />
      )}
      <div className="relative mr-60 w-full pb-2">
        <table className="w-11/12">
          <tbody>
            {user?.roles.some((role) => role === 'group_admin' || role === 'admin') && (
              <tr>
                <td className="w-1/3 py-2">Employee: </td>
                <td>
                  <div className="flex gap-1">{request.name}</div>
                </td>
              </tr>
            )}

            <tr>
              <td className="w-1/3 py-2">Period: </td>
              <td className="flex:wrap relative mt-1 flex w-1/6 bg-transparent" style={{ whiteSpace: 'nowrap' }}>
                {formattedStartDate + ' - ' + formattedEndDate}
              </td>
            </tr>
            <tr>
              <td className="w-1/3  py-2">Vacation type: </td>
              <td>{request.label}</td>
            </tr>
            <tr>
              <td className="w-1/3  py-2">Duration: </td>
              <td>{request.duration} days</td>
            </tr>
            <tr>
              <td className="w-1/3  py-2">Days left: </td>
              <td>{request.days_left} days</td>
            </tr>
            <tr>
              <td className="w-1/3  py-2"> Status: </td>
              <td>
                <div className="flex items-center justify-between align-top">
                  {request.status === 'approved' ? (
                    <img width="30" height="30" src={approved_request} />
                  ) : request.status === 'pending' || request.urgent ? (
                    <img width="30" height="30" src={pending_request} />
                  ) : (
                    <img width="30" height="30" src={declined_request} />
                  )}
                  {request.status}
                  <img
                    className="cursor-pointer dark:invert"
                    width="20"
                    height="20"
                    src={right_skinny_arrow}
                    onClick={() => handleClick()}
                  ></img>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}

export default RequestCard
