import React from 'react'
import { useLocation } from 'react-router-dom'
import { Request } from 'store/requestsSlice'
import RequestCard from 'components/RequestCard'
import RequestRow from 'components/RequestRow'
import { AuthUser } from 'store/authSlice'
import { getYear } from 'date-fns'

interface RequestProps {
  filteredRequests: Request[]
  user: AuthUser | null
  setAll?: (prev: any) => void
  all: boolean
  simple?: boolean
}
const RequestTable: React.FC<RequestProps> = ({ filteredRequests, user, all, setAll, simple }) => {
  const { pathname } = useLocation()
  return (
    <div>
      <div className="flex items-center justify-between bg-blue p-1 px-4 dark:bg-blue-dark">
        {user?.roles.some((role) => role === 'group_admin' || role === 'admin') ? (
          <div className="mx-auto text-center text-white">
            {simple ? `My ${getYear(new Date())} Requests` : 'Requests'}
          </div>
        ) : (
          <div className="mx-auto text-center text-white">
            {simple ? `My ${getYear(new Date())} Requests` : 'My requests'}
          </div>
        )}
        {user?.roles.some((role) => role === 'admin' || role === 'group_admin') && pathname !== '/calendar' && !simple && (
          <div className="hidden md:form-control md:w-24">
            <label className="label cursor-pointer">
              <span className="label-text text-white">All</span>
              <input
                onChange={(newValue) => {
                  if (setAll) {
                    setAll((prev: any) => !prev)
                  }
                }}
                type="checkbox"
                className="toggle toggle-primary"
              />
              <span className="label-text text-white">Me</span>
            </label>
          </div>
        )}
      </div>
      <div className=" w-full overflow-x-auto">
        <table className="hidden md:table-compact md:table md:w-full">
          <thead>
            <tr>
              {user?.roles.some((role) => role === 'admin' || role === 'group_admin') && !simple && (
                <th className="bg-amber-700 normal-case">Employee</th>
              )}
              <th>Period</th>
              {!simple && <th>Vacation type</th>}
              <th>Duration</th>
              {!simple && <th>Days left</th>}
              <th>Status</th>
            </tr>
          </thead>
          <tbody className="hidden border md:contents">
            {filteredRequests.map((request) => (
              <RequestRow key={request.id} request={request} simple={simple} />
            ))}
          </tbody>
        </table>
      </div>
      <div className="flex-col divide-y md:hidden">
        {filteredRequests.map((request) => (
          <RequestCard key={request.id} request={request} />
        ))}
      </div>
    </div>
  )
}

export default RequestTable
