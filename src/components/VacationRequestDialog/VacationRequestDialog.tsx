import React from 'react'
import { useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import { Request } from 'store/requestsSlice'
import { AuthUser } from 'store/authSlice'
import { useFormik } from 'formik'
import { InferType, object } from 'yup'
import Dialog from 'components/Dialog'

const vacationRequestDialogSchema = object({
  // todo
})

type FormValues = InferType<typeof vacationRequestDialogSchema>
interface VacationRequestDialogProps {
  dialogOpen: boolean
  toggleDialogOpen: () => void
  request: Request
}

const VacationRequestDialog: React.FC<VacationRequestDialogProps> = ({ dialogOpen, toggleDialogOpen, request }) => {
  const formik = useFormik({
    initialValues: vacationRequestDialogSchema.getDefault(),
    validationSchema: vacationRequestDialogSchema,
    onSubmit: async (values: FormValues) => {}
  })

  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)

  const title =
    request.user_id === user?.id
      ? 'My Vacation Request'
      : request.cancellation
      ? 'Vacation Request Cancellation'
      : 'Vacation Request'

  const newState =
    request.user_id === user?.id ? (request.cancellation ? 'EDIT' : 'ADMIN_VIEW_CANCELLATION') : 'ADMIN_VIEW'

  return (
    <>
      {dialogOpen && (
        <Dialog title={title} onClose={toggleDialogOpen}>
          <form onSubmit={formik.handleSubmit}></form>
        </Dialog>
      )}
    </>
  )
}
export default VacationRequestDialog
