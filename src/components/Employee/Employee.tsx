import React from 'react'
import { EmployeeUser } from 'store/employeesSlice'
import default_profile_image from 'assets/assets/profile.png'
import cx from 'classnames'

interface EmployeeProps {
  employee: EmployeeUser
  dashboardVersion?: boolean
}

const Employee: React.FC<EmployeeProps> = ({ employee, dashboardVersion }) => {
  return (
    <div
      className={cx('flex flex-col items-center', {
        'w-40': !dashboardVersion,
        'w-52': dashboardVersion
      })}
    >
      <div className="avatar mx-6 mb-3">
        <div
          className={cx('rounded-full', {
            'w-24': !dashboardVersion,
            'w-36': dashboardVersion
          })}
        >
          <img src={employee.profile_image ?? default_profile_image} />
        </div>
      </div>
      <div className="text-center text-sm">{employee.name}</div>
      <div className="text-center text-xs text-blue">{employee.job_title}</div>
    </div>
  )
}

export default Employee
