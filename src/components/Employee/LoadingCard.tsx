import React, { ReactElement } from 'react'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

const LoadingCard: React.FC = (): ReactElement => (
  <div className="w-40 lg:w-fit">
    <div className="avatar mx-6">
      <div className="w-24 rounded-full">
        <Skeleton circle height="100%" />
      </div>
    </div>
    <div className="text-center text-sm">
      <Skeleton />
    </div>
    <div className="text-center text-xs text-blue">
      <Skeleton />
    </div>
  </div>
)

export default LoadingCard
