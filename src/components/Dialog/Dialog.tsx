import React, { ReactNode } from 'react'

interface DialogProps {
  title: string
  onClose: Function
  children?: ReactNode
}

const Dialog: React.FC<DialogProps> = ({ title, onClose, children }) => {
  return (
    <div>
      <input type="checkbox" id="dialogBox" className="modal-toggle" defaultChecked={true} onChange={() => onClose()} />
      <label htmlFor="dialogBox" className="modal backdrop-blur-sm modal-bottom sm:modal-middle sm:rounded-none cursor-pointer" onReset={() => onClose()}>
        <label
          className="modal-box relative min-h-[200px] min-w-[350px]
          sm:rounded-none bg-white px-4 py-4 text-black shadow-lg dark:bg-font-main
          dark:text-very-light-gray"
          htmlFor=""
        >
          <h3 className="mb-3 text-center">{title}</h3>
          {children}
        </label>
      </label>
    </div>
  )
}

export default Dialog
