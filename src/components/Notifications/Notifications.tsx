import React from 'react'
import cx from 'classnames'
import { deleteAllNotifications, deleteOneNotification, Notification } from 'store/notifcationsSlice'
import { ReactComponent as NotificationIcon } from 'assets/icons/notifications.svg'
import { ReactComponent as BackArrow } from 'assets/icons/left_skinny_arrow.svg'
import 'routing/Layout'
import { RootState } from 'store/store'
import { useAppDispatch, useAppSelector } from 'store/hooks'

interface NotificationsProps {
  notificationOpen: boolean
  toggleNotifications: () => void
  notifications: Notification[]
}

const Notifications: React.FC<NotificationsProps> = (props: NotificationsProps) => {
  const dispatch = useAppDispatch()

  const errorMessage = useAppSelector((state: RootState): string | null => state.notifications.errorMessage)
  const errorNotificationId = useAppSelector(
    (state: RootState): number | null => state.notifications.errorNotificationId
  )

  const handleDeleteOneNotification = (id: number): void => {
    void dispatch(deleteOneNotification(id))
  }

  const handleDeleteAllNotifications = (): void => {
    void dispatch(deleteAllNotifications())
  }

  const notificationsArray = props.notifications.map((notification) => {
    const date = new Date(notification.created_at)
    const formattedDate = `${date.toLocaleDateString('en-US', {
      month: 'short',
      day: 'numeric'
    })} ${date.toLocaleDateString('en-US', {
      year: 'numeric'
    })}, ${date
      .toLocaleTimeString('en-US', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
        hourCycle: 'h12'
      })
      .replace(/\s(AM|PM)$/, '$1')
      .toLowerCase()}`

    return (
      <div key={notification.id} className="flex flex-col justify-end border-b py-2 px-5 pr-3 md:px-0">
        <p className="pr-3 pt-1 text-right text-[13px] text-gray-400 md:pt-0 md:pr-0">{formattedDate}</p>
        <p className="pt-2 pb-2 text-[15px] lg:text-[14px]">{notification.text}</p>
        <button
          className="ml-auto pr-3 text-[14px] text-blue md:pr-0"
          onClick={() => handleDeleteOneNotification(notification.id)}
        >
          Dismiss
        </button>
        {errorMessage && notification.id === errorNotificationId && (
          <div className="pr-3 text-right text-red">{errorMessage}</div>
        )}
      </div>
    )
  })
  return (
    <>
      <div
        className={cx(
          'absolute right-0 top-0 z-50 h-screen w-full -translate-y-full bg-white p-4 drop-shadow transition duration-200 dark:bg-font-main sm:h-auto sm:w-[300px]',
          { 'translate-y-0': props.notificationOpen },
          { 'overflow-hidden': !props.notificationOpen }
        )}
      >
        <div className="flex items-center gap-2">
          <BackArrow onClick={props.toggleNotifications} className="ml-3 h-4 w-4 dark:invert md:hidden" />
          <NotificationIcon onClick={props.toggleNotifications} className="h-8 w-8 cursor-pointer dark:invert" />
          {props.notifications.length > 0 && (
            <div className="absolute left-[68px] top-3.5 z-50 flex h-3.5 w-3.5 items-center justify-center rounded-full bg-blue md:top-3.5 md:left-8">
              <span className="text-[11px] text-white">{props.notifications.length}</span>
            </div>
          )}
          <p className="text-[15px]">Notifications</p>
          <div className="z-50 ml-auto mr-1 md:-mb-8 md:mr-3">
            {props.notifications.length < 0 && (
              <button className="pr-3 text-[14px] text-blue" onClick={() => handleDeleteAllNotifications()}>
                Dismiss All
              </button>
            )}
            {errorMessage && (
              <div className="absolute top-9 text-right text-right text-red md:top-4">{errorMessage}</div>
            )}
          </div>
        </div>
        <div className="relative top-4 border-b drop-shadow md:drop-shadow-none lg:ml-1" />
        <div className="relative top-4 max-h-[100%] min-h-[50px] overflow-x-hidden overflow-y-scroll scrollbar-hide md:max-h-[500px]">
          {props.notifications.length > 0 ? (
            notificationsArray
          ) : (
            <p className="pt-3 text-center">You have no notifications.</p>
          )}
        </div>
      </div>
      {props.notificationOpen && (
        <div onClick={props.toggleNotifications} className="absolute z-40 h-full w-full bg-black opacity-20"></div>
      )}
    </>
  )
}

export default Notifications
