import React, { useState, useRef, useEffect, useContext } from 'react'
import { useMedia } from 'react-use'
import CalendarHeader from 'components/Calendar/CalendarHeader'
import {Request, fetchRequests} from 'store/requestsSlice'
import { ThemeContext } from 'routing/Layout'
import { useAppSelector, useAppDispatch } from 'store/hooks'
import { fetchTeams, Team } from 'store/teamsSlice'
import { EmployeeUser, fetchEmployees } from 'store/employeesSlice'
import { setWeekStart, fetchNonWorkingDays } from 'store/calendarSlice'
import { RootState } from 'store/store'
import FullCalendar from '@fullcalendar/react'
import { CalendarApi, EventClickArg, DayCellContentArg, DateSelectArg } from '@fullcalendar/core'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import './calendar.css'
import RequestTable from 'components/RequestTable'
import {AuthUser} from 'store/authSlice'

const Calendar: React.FC = () => {
  const [allRequestsDisplay, setAllRequestsDisplay] = useState(true)
  const [title, setTitle] = useState('')
  const calendarRef = useRef<FullCalendar>(null)
  const calendarApi: CalendarApi | undefined = calendarRef.current?.getApi()
  const dispatch = useAppDispatch()

  const isSmallScreen = useMedia('(max-width: 768px)')

  const teams = useAppSelector((state: RootState): Team[] => state.teams.teams)
  const allRequests = useAppSelector((state: RootState): Request[] => state.requests.requests)
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)
  const nonWorkingDays = useAppSelector((state: RootState) => state.calendar.nonWorkingDates)
  const weekStart = useAppSelector((state: RootState) => state.calendar.weekStart)
  const allEmployees = useAppSelector((state: RootState): EmployeeUser[] => state.employees.employees)
  const weekStartInternalValue = window.localStorage.getItem('weekStart')

  useEffect(() => {
    void dispatch(fetchTeams())
    void dispatch(fetchNonWorkingDays())
    void dispatch(fetchEmployees())
    if (weekStartInternalValue !== null) {
      void dispatch(setWeekStart(JSON.parse(weekStartInternalValue)))
    }
    if (calendarRef.current) {
      setTitle(calendarRef.current.getApi().view.title)
    }
  }, [])

  useEffect(() => {
    void dispatch(fetchRequests({ all: allRequestsDisplay }))
  }, [allRequestsDisplay])

  const { myThemeState, setMyThemeState } = useContext(ThemeContext)

  const events = allRequests.map((request) => {
    const user = allEmployees.find((employee) => employee.id === request.user_id)
    return {
      id: request?.id.toString(),
      title: user ? user.name : '',
      start: request.start_date,
      end: request.end_date,
      textColor: myThemeState === 'light' ? 'black' : 'white',
      classNames: [`event-class-${myThemeState as string}`]
    }
  })

  const handlePrev = (): void => {
    if (calendarRef.current) {
      calendarRef.current.getApi().prev()
      setTitle(calendarRef.current.getApi().view.title)
    }
  }

  const handleNext = (): void => {
    if (calendarRef.current) {
      calendarRef.current.getApi().next()
      setTitle(calendarRef.current.getApi().view.title)
    }
  }

  const getDayCellClassNames = (arg: DayCellContentArg): string[] => {
    const classNames = []
    if (arg.date.getDay() === 0 || arg.date.getDay() === 6) {
      classNames.push(`weekend-${myThemeState as string}`)
      if (arg.date.toDateString() === new Date().toDateString()) {
        classNames.push(`current-day-weekend-${myThemeState as string}`)
      }
    }
    nonWorkingDays.forEach((nonWorkingDay) => {
      const nonWorkingDayDate = new Date(nonWorkingDay.date)
      if (arg.date.toDateString() === nonWorkingDayDate.toDateString()) {
        classNames.push(`non-working-day-${myThemeState as string}`)
        if (arg.date.toDateString() === new Date().toDateString()) {
          classNames.push(`current-day-non-working-${myThemeState as string}`)
        }
      }
      if (arg.date.toDateString() === new Date().toDateString()) {
        classNames.push(`current-day-${myThemeState as string}`)
      }
    })
    return classNames
  }

  const getNonWorkingDaysHeaderText = (arg: DayCellContentArg): string => {
    let text = `${arg.date.getDate()}`
    nonWorkingDays.forEach((nonWorkingDay) => {
      const nonWorkingDayDate = new Date(nonWorkingDay.date)
      if (arg.date.toDateString() === nonWorkingDayDate.toDateString()) {
        text = `${arg.date.getDate()} Non-working day`
      }
    })
    return text
  }

  const handleEvent = (prop: EventClickArg): void => {
    console.log(prop.event.id)
  }

  const handleDateSelect = (selectInfo: DateSelectArg): void => {
      console.log(`${selectInfo.start.toDateString()} - ${selectInfo.end.toDateString()}`)
  }

  return (
    <main className="sm:pt-10">
      <CalendarHeader
        teams={teams}
        weekStartInternalValue={weekStartInternalValue}
        title={title}
        handlePrev={handlePrev}
        handleNext={handleNext}
        allRequestsDisplay={allRequestsDisplay}
        setAllRequestsDisplay={setAllRequestsDisplay}
      />
      <FullCalendar
        select={handleDateSelect}
        dayHeaderClassNames={[`header-class-${myThemeState as string}`]}
        dayCellContent={getNonWorkingDaysHeaderText}
        dayCellClassNames={getDayCellClassNames}
        ref={calendarRef}
        eventClick={handleEvent}
        headerToolbar={false}
        plugins={[dayGridPlugin, interactionPlugin]}
        events={events}
        selectable={true}
        editable={true}
        slotLabelInterval="ddd"
        fixedWeekCount={false}
        handleWindowResize={true}
        firstDay={weekStart}
        height={isSmallScreen ? '100vh' : '90vh'}
      />
      <div className='pt-10'>
        <RequestTable filteredRequests={allRequests} user={user} all={false}/>
      </div>
    </main>
  )
}

export default Calendar
