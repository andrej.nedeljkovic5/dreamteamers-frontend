import React, { ReactElement } from 'react'
import Button from 'components/Button'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select from '@mui/material/Select'

interface MobileHedarProps {
  selectedTeamsNames: string[]
  selectedTeams: any[]
  teamsArray: ReactElement[]
  allRequestsDisplay: boolean
  setAllRequestsDisplay: React.Dispatch<React.SetStateAction<boolean>>
}

const MobileHeader: React.FC<MobileHedarProps> = (props: MobileHedarProps) => {
  const [showSelectDropdownMobile, setShowSelectDropdownMobile] = React.useState(false)

  const handleSaveTeamsMobile = (): void => {
    console.log(props.selectedTeamsNames)
    setShowSelectDropdownMobile(false)
  }

  return (
    <div className="flex h-[35px] flex-row items-center justify-between bg-white dark:bg-font-main sm:hidden">
      <FormControl variant="standard" sx={{ m: 2, minWidth: 90, minHeight: 20, pr: 1, pb: 1.5 }} className="">
        <InputLabel className="dark:invert" shrink={false} id="dropdown">
          Select Team
        </InputLabel>
        <Select
          open={showSelectDropdownMobile}
          onOpen={() => setShowSelectDropdownMobile(true)}
          onClose={() => setShowSelectDropdownMobile(false)}
          multiple={true}
          sx={{ width: 115 }}
          disableUnderline={true}
          labelId="dropdown"
          className="dark:invert"
          MenuProps={{
            disableEnforceFocus: true,
            PaperProps: {
              style: {
                maxWidth: '350px',
                minWidth: '250px'
              }
            }
          }}
          inputProps={{ underline: 'none' }}
          defaultValue={[]}
          value={props.selectedTeams ?? []}
        >
          {props.teamsArray}
          <div className="flex w-full items-center justify-center">
            <Button className="" onClick={() => handleSaveTeamsMobile()}>
              Save
            </Button>
          </div>
        </Select>
      </FormControl>

      <div className="flex flex-row gap-1 pr-2">
        <span className="mr-1 text-[16px] text-black dark:text-white sm:hidden">All</span>
        <label
          htmlFor="default-toggle-calendar-mobile"
          className="relative inline-flex cursor-pointer items-center sm:hidden"
        >
          <input
            name="meAll"
            type="checkbox"
            checked={!props.allRequestsDisplay}
            onChange={() => props.setAllRequestsDisplay(!props.allRequestsDisplay)}
            id="default-toggle-calendar-mobile"
            className="peer sr-only sm:hidden"
          />
          <div className="peer-checked:bg-blue-600 peer h-5 w-9 rounded-full bg-gray-200 after:absolute after:top-[4px] after:left-[2px] after:h-4 after:w-4 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:outline-none dark:border-gray-600 dark:bg-gray-700 sm:hidden" />
        </label>
        <span className="ml-1 text-[16px] text-black dark:text-white sm:hidden">Me</span>
      </div>
    </div>
  )
}

export default MobileHeader
