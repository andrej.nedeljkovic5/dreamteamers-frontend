import React, { useEffect, useState } from 'react'
import Button from 'components/Button'
import MobileHeader from 'components/Calendar/MobileHeader'
import { setWeekStart } from 'store/calendarSlice'
import { Team, fetchTeams } from 'store/teamsSlice'
import { useAppDispatch } from 'store/hooks'
import Checkbox from '@mui/material/Checkbox'
import { ListItemText, RadioGroup, Radio, FormControlLabel } from '@mui/material'
import Select from '@mui/material/Select'
import FormControl from '@mui/material/FormControl'
import MenuItem from '@mui/material/MenuItem'
import { ReactComponent as LeftArrow } from 'assets/icons/left_arrow.svg'
import { ReactComponent as RightArrow } from 'assets/icons/right arrow.svg'

interface HeaderProps {
  teams: Team[]
  title: string
  handlePrev: () => void
  handleNext: () => void
  allRequestsDisplay: boolean
  setAllRequestsDisplay: React.Dispatch<React.SetStateAction<boolean>>
  weekStartInternalValue: string | null
}

const CalendarHeader: React.FC<HeaderProps> = ({
  teams,
  title,
  handlePrev,
  handleNext,
  allRequestsDisplay,
  setAllRequestsDisplay,
  weekStartInternalValue
}: HeaderProps) => {
  const [showSelectDropdown, setShowSelectDropdown] = useState(false)
  const [selectedTeamsNames, setSelectedTeamsNames] = useState<string[]>([])
  const [selectedTeams, setSelectedTeams] = useState<boolean[]>(new Array(teams.length).fill(false))
  const [selectedWeekStart, setSelectedWeekStart] = useState<'Sunday' | 'Monday'>('Monday')
  const dispatch = useAppDispatch()

  const handleChange = (position: number): void => {
    setSelectedTeams((prevState) => prevState.map((selected, index) => (index === position ? !selected : selected)))

    setSelectedTeamsNames((prevState) => {
      const name = teams[position].name
      return prevState.includes(name) ? prevState.filter((teamName) => teamName !== name) : [...prevState, name]
    })
  }

  useEffect(() => {
    void dispatch(fetchTeams())
    if (weekStartInternalValue !== null) {
      if (weekStartInternalValue === '0') {
        setSelectedWeekStart('Sunday')
      } else {
        setSelectedWeekStart('Monday')
      }
    }
  }, [])

  const teamsArray = teams.map((group, index) => {
    return (
      <MenuItem key={group.id} defaultValue="">
        <Checkbox
          name={group.name}
          defaultValue=""
          value={group.name}
          checked={selectedTeams[index]}
          onChange={() => handleChange(index)}
        />
        <ListItemText primary={group.name} />
      </MenuItem>
    )
  })

  const handleFilterTeam = (): void => {
    console.log(selectedTeamsNames)
    setShowSelectDropdown(false)
  }

  return (
    <>
      <MobileHeader
        selectedTeamsNames={selectedTeamsNames}
        selectedTeams={selectedTeams}
        teamsArray={teamsArray}
        allRequestsDisplay={allRequestsDisplay}
        setAllRequestsDisplay={setAllRequestsDisplay}
      />
      <header>
        <div className="flex h-[35px] flex-row items-center justify-between bg-blue dark:bg-blue-dark sm:h-10">
          <div className="md:w-[370px]">
            <LeftArrow className="ml-2 h-5 w-5 cursor-pointer invert" onClick={handlePrev} />
          </div>
          <p className="text-[16px] text-white">{title}</p>
          <div className="mr-1 flex flex-row items-center gap-1 md:w-[370px]">
            <div className="relative hidden md:block">
              <label className="absolute top-[50%] -mt-[12px] text-center text-white">Select Team</label>
              <FormControl variant="standard" sx={{ width: 115 }}>
                <Select
                  open={showSelectDropdown}
                  onOpen={() => setShowSelectDropdown(true)}
                  onClose={() => setShowSelectDropdown(false)}
                  multiple={true}
                  disableUnderline={true}
                  className="invert"
                  MenuProps={{
                    disableEnforceFocus: true,
                    PaperProps: {
                      style: {
                        maxWidth: '300px',
                        minWidth: '200px'
                      }
                    }
                  }}
                  inputProps={{ underline: 'none' }}
                  label="Team"
                  value={selectedTeamsNames ?? []}
                >
                  {teamsArray}
                  <div className="flex w-full items-center justify-center">
                    <Button className="" onClick={handleFilterTeam}>
                      Save
                    </Button>
                  </div>
                </Select>
              </FormControl>
            </div>
            <span className="mr-1 hidden text-[16px] text-white sm:block">All</span>
            <label
              htmlFor="default-toggle-calendar"
              className="relative inline-flex hidden cursor-pointer items-center sm:block"
            >
              <input
                name="meAll"
                type="checkbox"
                checked={!allRequestsDisplay}
                onChange={() => setAllRequestsDisplay(!allRequestsDisplay)}
                id="default-toggle-calendar"
                className="peer sr-only hidden sm:block"
              />
              <div className="peer-checked:bg-blue-600 peer hidden h-5 w-9 rounded-full bg-gray-200 after:absolute after:top-[2px] after:left-[2px] after:h-4 after:w-4 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:outline-none dark:border-gray-600 dark:bg-gray-700 sm:block" />
            </label>
            <span className="ml-1 hidden text-[16px] text-white sm:block">Me</span>
            <div className="relative hidden pl-2 md:block">
              <label className="absolute top-[50%] -mt-[12px] text-center text-white">Month Start</label>
              <FormControl variant="standard">
                <Select
                  sx={{ width: 90, fill: 'white' }}
                  disableUnderline={true}
                  labelId="dropdown"
                  className="ml-6 invert"
                  MenuProps={{ disableEnforceFocus: true }}
                  inputProps={{ underline: 'none' }}
                  value=""
                >
                  <RadioGroup
                    style={{ paddingLeft: '10px' }}
                    aria-label="week-start"
                    name="week-start"
                    defaultValue=""
                    value={selectedWeekStart ?? ''}
                    onChange={(event) => {
                      setSelectedWeekStart(event.target.value as 'Sunday' | 'Monday')
                      dispatch(setWeekStart(event.target.value === 'Sunday' ? 0 : 1))
                    }}
                  >
                    <FormControlLabel value="Sunday" control={<Radio />} label="Sunday" />
                    <FormControlLabel value="Monday" control={<Radio />} label="Monday" />
                  </RadioGroup>
                </Select>
              </FormControl>
            </div>
            <RightArrow className="h-5 w-5 cursor-pointer invert" onClick={handleNext} />
          </div>
        </div>
      </header>
    </>
  )
}

export default CalendarHeader
