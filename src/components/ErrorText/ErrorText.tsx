import React from 'react'

interface ErrorTextProps {
  children?: string | string[] | never[] | false | undefined
}

const ErrorText: React.FC<ErrorTextProps> = ({ children }) => (
  <div className="min-h-4 text-end text-xs text-red">{children}</div>
)

export default ErrorText
