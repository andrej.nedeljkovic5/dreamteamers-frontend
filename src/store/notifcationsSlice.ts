import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'services/api/apiCommon'

export interface Notification {
  id: number
  text: string
  created_at: string
  user_id: number
}

export interface NotificationsState {
  notifications: Notification[]
  status: 'empty' | 'idle' | 'loading' | 'failed'
  errorMessage: string | null
  errorNotificationId: number | null
}

export const initialState: NotificationsState = {
  notifications: [],
  status: 'empty',
  errorMessage: null,
  errorNotificationId: null
}

export const getNotifications = createAsyncThunk<Notification[]>('notifications/getAll', async () => {
  const res = await axios.get('notifications')
  return res
})

export const deleteOneNotification = createAsyncThunk<
  { id: number; response: { notificationsDismissed: boolean } },
  number,
  {
    rejectValue: { error: string }
  }
>('notifications/deleteOne', async (id: number, { rejectWithValue }) => {
  try {
    const response = await axios.delete(`notifications/${id}`)
    return {
      id,
      response: response.data
    }
  } catch (error: any) {
    return rejectWithValue({ error: 'Server error' })
  }
})

export const deleteAllNotifications = createAsyncThunk<
  undefined,
  undefined,
  {
    rejectValue: { error: string }
  }
>('notifications/deleteAll', async (_, { rejectWithValue }) => {
  try {
    await axios.delete('notifications/')
  } catch (error: any) {
    return rejectWithValue({ error: 'Server error' })
  }
})

export const notificationsSlice = createSlice({
  name: 'notifications',
  reducers: {},
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getNotifications.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(getNotifications.fulfilled, (state, action) => {
        state.status = 'idle'
        state.notifications = [...action.payload, ...state.notifications]
      })
      .addCase(getNotifications.rejected, (state) => {
        state.status = 'failed'
      })
      .addCase(deleteOneNotification.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(deleteOneNotification.fulfilled, (state, action) => {
        state.status = 'idle'
        state.notifications = state.notifications.filter((notification) => notification.id !== action.payload.id)
        state.errorMessage = null
      })
      .addCase(deleteOneNotification.rejected, (state, action: any) => {
        state.status = 'failed'
        state.errorMessage = action.payload.error
        state.errorNotificationId = action.meta.arg
      })
      .addCase(deleteAllNotifications.rejected, (state, action: any) => {
        state.status = 'failed'
        state.errorMessage = action.payload.error
      })
      .addCase(deleteAllNotifications.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(deleteAllNotifications.fulfilled, (state) => {
        state.notifications = []
        state.status = 'idle'
        state.errorMessage = null
      })
  }
})

export default notificationsSlice.reducer
