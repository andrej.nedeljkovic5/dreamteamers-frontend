import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import api from 'services/api'
import { ValidationError, ValidationMessage } from 'services/api/apiCommon'

export interface EmployeeUser {
  id: number
  name: string
  job_title?: string
  profile_image?: string | null
}

export interface EmployeesState {
  employees: EmployeeUser[]
  fetchEmployeesStatus: 'empty' | 'idle' | 'loading' | 'failed'
  saveUserStatus: 'empty' | 'idle' | 'loading' | 'failed'
  apiError: string
  appInitializedStatus: boolean
  validationErrors: ValidationMessage[]
}

interface SaveUserData {
  id?: number
  name: string
  email: string
  roleInit: string
  position: string
  employedSince: Date | null
  freeDays: string
  selectedTeams: string[]
}

const initialState: EmployeesState = {
  employees: [],
  fetchEmployeesStatus: 'empty',
  saveUserStatus: 'empty',
  appInitializedStatus: false,
  validationErrors: [],
  apiError: ''
}

export const saveUser = createAsyncThunk<EmployeeUser, SaveUserData, { rejectValue: ValidationMessage[] | string }>(
  'users/save-user',
  async (saveUserData, { rejectWithValue }) => {
    try {
      return await api.users
        .saveUser(
          saveUserData.name,
          saveUserData.email,
          saveUserData.roleInit,
          saveUserData.position,
          saveUserData.employedSince,
          saveUserData.freeDays,
          saveUserData.selectedTeams
        )
        .then((id) => {
          return { ...saveUserData, id, job_title: saveUserData.position }
        })
    } catch (err: any) {
      if (err instanceof ValidationError) {
        return rejectWithValue(err.messages)
      } else {
        return rejectWithValue(err.message)
      }
    }
  }
)

export const fetchEmployees = createAsyncThunk<EmployeeUser[], number | undefined>(
  'employees/getEmployees',
  async (teamId?: number) => {
    return await api.users.getEmployees(teamId)
  }
)

export const EmployeesSlice = createSlice({
  name: 'employees',
  initialState,
  reducers: {
    clearEmployees: (state) => {
      state.employees = []
      state.fetchEmployeesStatus = 'empty'
    },
    resetSaveUserStatus: (state) => {
      state.saveUserStatus = 'empty'
    },
    clearApiError: (state) => {
      state.apiError = ''
    },
    clearValidationErrors: (state) => {
      state.validationErrors = []
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchEmployees.pending, (state) => {
        state.fetchEmployeesStatus = 'loading'
      })
      .addCase(fetchEmployees.fulfilled, (state, action) => {
        state.fetchEmployeesStatus = 'idle'
        state.employees = action.payload
      })
      .addCase(fetchEmployees.rejected, (state) => {
        state.fetchEmployeesStatus = 'empty'
      })
      .addCase(saveUser.pending, (state) => {
        state.saveUserStatus = 'loading'
      })
      .addCase(saveUser.fulfilled, (state, action) => {
        state.saveUserStatus = 'idle'
        state.validationErrors = []
        state.employees.push(action.payload)
        state.apiError = ''
      })
      .addCase(saveUser.rejected, (state, action) => {
        state.saveUserStatus = 'empty'
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
      })
  }
})

export const { clearValidationErrors, clearApiError, clearEmployees, resetSaveUserStatus } = EmployeesSlice.actions

export default EmployeesSlice.reducer
