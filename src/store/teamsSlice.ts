import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import api from 'services/api'

export interface Team {
  id: number
  name: string
  color?: string
  admins?: string[]
  employeesCount?: number
}

export interface TeamsState {
  teams: Team[]
  status: 'empty' | 'idle' | 'loading' | 'failed'
}

const initialState: TeamsState = {
  teams: [],
  status: 'empty'
}

export const fetchTeams = createAsyncThunk<Team[], boolean | undefined>('teams/fetch', async (details?) => {
  return await api.teams.getTeams(details)
})

export const teamsSlice = createSlice({
  name: 'teams',
  initialState,
  reducers: {
    clearTeams: (state) => {
      state.teams = []
      state.status = 'empty'
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchTeams.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchTeams.fulfilled, (state, action) => {
        state.status = 'idle'
        state.teams = action.payload
      })
      .addCase(fetchTeams.rejected, (state) => {
        state.status = 'empty'
      })
  }
})

export const { clearTeams } = teamsSlice.actions

export default teamsSlice.reducer
