import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import api from 'services/api'
import axios, { setAccessToken, ValidationError, ValidationMessage } from 'services/api/apiCommon'

export interface UserData {
  name: string
  email: string
  phone_number?: string | null
  date_of_birth?: Date | null
  job_title?: string
  employed_since?: Date | null
  days_left?: number
  days_assigned?: number
  profile_image?: string | null
}

export interface AuthUser extends UserData {
  id: number
  roles: Array<'admin' | 'group_admin' | 'employee'>
  teams?: string[]
  token?: string
}

export interface AuthState {
  user: AuthUser | null
  status: 'empty' | 'idle' | 'loading' | 'failed'
  userDataStatus: 'empty' | 'idle' | 'loading' | 'failed'
  userExistsStatus: 'empty' | 'idle' | 'loading' | 'failed'
  loginStatus: 'empty' | 'idle' | 'loading' | 'failed'
  userExists: boolean
  appInitializedStatus: boolean
  emailSent: boolean
  passwordChanged: boolean
  validationErrors: ValidationMessage[]
  apiError: string
}

const initialState: AuthState = {
  user: null,
  status: 'empty',
  userDataStatus: 'empty',
  userExistsStatus: 'empty',
  loginStatus: 'empty',
  userExists: false,
  appInitializedStatus: false,
  emailSent: false,
  passwordChanged: false,
  validationErrors: [],
  apiError: ''
}

export const findUser = createAsyncThunk<
  { success: boolean },
  { email: string; token: string },
  { rejectValue: ValidationMessage[] }
>('fetchUser', async ({ email, token }: { email: string; token: string }, { rejectWithValue }) => {
  try {
    const response = await axios.get(`/auth/find-user?email=${email}&token=${token}`)
    return { success: response }
  } catch (err: any) {
    const error: ValidationError = err
    return rejectWithValue(error.messages)
  }
})

export const updatePassword = createAsyncThunk<
  { passwordChanged: boolean },
  {
    password: string
    email: string
  },
  { rejectValue: ValidationMessage[] | string }
>(
  'auth/update-password',
  async (
    updatePasswordData: {
      password: string
      email: string
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await axios.post('/auth/register', {
        email: updatePasswordData.email,
        password: updatePasswordData.password
      })
      if (response.data) {
        return { passwordChanged: true }
      }
      throw new Error('An error occured while updating the password')
    } catch (err: any) {
      if (err instanceof ValidationError) {
        return rejectWithValue(err.messages)
      } else {
        return rejectWithValue(err.message)
      }
    }
  }
)

export const login = createAsyncThunk<
  AuthUser,
  {
    email: string
    password: string
  },
  { rejectValue: ValidationMessage[] }
>(
  'auth/login',
  async (
    loginData: {
      email: string
      password: string
    },
    { rejectWithValue }
  ) => {
    try {
      return await api.auth.login(loginData.email, loginData.password)
    } catch (err: any) {
      if (err instanceof ValidationError) {
        return rejectWithValue(err.messages)
      } else {
        return rejectWithValue(err.message)
      }
    }
  }
)

export const forgotPassword = createAsyncThunk<
  { tokenGenerated: boolean; emailSent: boolean },
  {
    email: string
  },
  { rejectValue: ValidationMessage[] | string }
>(
  'auth/forgot-password',
  async (
    forgotPasswordData: {
      email: string
    },
    { rejectWithValue }
  ) => {
    try {
      return await api.auth.forgotPassword(forgotPasswordData.email)
    } catch (err: any) {
      if (err instanceof ValidationError) {
        return rejectWithValue(err.messages)
      } else {
        return rejectWithValue(err.message)
      }
    }
  }
)

export const resetPassword = createAsyncThunk<
  { passwordChanged: boolean },
  {
    password: string
    email: string
    token: string
  },
  { rejectValue: ValidationMessage[] | string }
>(
  'auth/reset-password',
  async (
    resetPasswordData: {
      password: string
      email: string
      token: string
    },
    { rejectWithValue }
  ) => {
    try {
      return await api.auth.resetPassword(resetPasswordData.password, resetPasswordData.email, resetPasswordData.token)
    } catch (err: any) {
      if (err instanceof ValidationError) {
        return rejectWithValue(err.messages)
      } else {
        return rejectWithValue(err.message)
      }
    }
  }
)

export const fetchDashboardData = createAsyncThunk<UserData>(
  'auth/dashboard-data',
  async () => await api.users.getAdditionalData()
)

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    addTeam: ({ user }, action: PayloadAction<string>) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      user = user
        ? {
            ...user,
            teams: user.teams ? [...user.teams, action.payload] : [action.payload]
          }
        : null
    },
    logout: (state) => {
      state.user = null
      localStorage.removeItem('user')
      setAccessToken()
    },
    cleanErrors: (state) => {
      state.validationErrors = []
    },
    initializeApp: (state) => {
      const localUser: string | null = localStorage.getItem('user')

      if (localUser && !state.user) {
        state.status = 'idle'
        const obj = JSON.parse(localUser)
        setAccessToken(obj.token)
        state.user = { ...obj }
        state.validationErrors = []
      }
      state.appInitializedStatus = true
    }
  },
  // The `extraReducers` field lets the slice handle actions defined elsewhere,
  // including actions generated by createAsyncThunk or in other slices.
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        state.status = 'loading'
        state.loginStatus = 'loading'
      })
      .addCase(login.fulfilled, (state, action) => {
        state.status = 'idle'
        state.validationErrors = []
        state.apiError = ''
        setAccessToken(action.payload?.token ?? '')
        localStorage.setItem('user', JSON.stringify(action.payload))
        state.user = action.payload
        state.appInitializedStatus = true
        state.loginStatus = 'idle'
      })
      .addCase(login.rejected, (state, action) => {
        state.status = 'empty'
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
        state.loginStatus = 'failed'
      })
      .addCase(fetchDashboardData.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchDashboardData.fulfilled, (state, action) => {
        if (state.user) {
          state.status = 'idle'
          state.user = {
            ...state.user,
            ...action.payload,
            date_of_birth: action.payload.date_of_birth ? new Date(action.payload.date_of_birth) : undefined
          }
        } else {
          state.status = 'empty'
        }
      })
      .addCase(fetchDashboardData.rejected, (state, action) => {
        state.status = 'failed'
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
        state.loginStatus = 'failed'
      })
      .addCase(forgotPassword.fulfilled, (state, action) => {
        state.status = 'empty'
        state.validationErrors = []
        state.emailSent = true
      })
      .addCase(forgotPassword.rejected, (state, action) => {
        state.status = 'empty'
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
      })
      .addCase(forgotPassword.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(resetPassword.fulfilled, (state, action) => {
        state.passwordChanged = true
        state.status = 'empty'
        state.validationErrors = []
      })
      .addCase(resetPassword.rejected, (state, action) => {
        state.status = 'empty'
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
      })
      .addCase(resetPassword.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(findUser.pending, (state) => {
        state.status = 'loading'
        state.userExistsStatus = 'loading'
      })
      .addCase(findUser.fulfilled, (state, action) => {
        state.status = 'idle'
        state.validationErrors = []
        state.userExists = action.payload.success
        state.userExistsStatus = 'idle'
      })
      .addCase(findUser.rejected, (state, action) => {
        state.validationErrors = action.payload as ValidationMessage[]
        state.status = 'empty'
        state.userExistsStatus = 'failed'
      })
      .addCase(updatePassword.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(updatePassword.fulfilled, (state, action) => {
        state.status = 'idle'
        state.validationErrors = []
        state.passwordChanged = true
      })
      .addCase(updatePassword.rejected, (state, action) => {
        console.log(action.payload)
        typeof action.payload === 'string'
          ? (state.apiError = action.payload)
          : (state.validationErrors = action.payload as ValidationMessage[])
        state.status = 'empty'
      })
  }
})

export const { logout, initializeApp, cleanErrors } = authSlice.actions

export default authSlice.reducer
