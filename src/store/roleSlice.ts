import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import api from 'services/api'

export interface Role {
  id: number
  name: string
}

export interface RoleState {
  roles: Role[]
  status: 'empty' | 'idle' | 'loading' | 'failed'
}

const initialState: RoleState = {
  roles: [],
  status: 'empty'
}

export const fetchRoles = createAsyncThunk<Role[]>('roles/fetch', async () => {
  return await api.users.getRoles()
})

export const rolesSlice = createSlice({
  name: 'roles',
  initialState,
  reducers: {
    clearRoles: (state) => {
      state.roles = []
      state.status = 'empty'
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRoles.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchRoles.fulfilled, (state, action) => {
        state.status = 'idle'
        state.roles = action.payload
      })
      .addCase(fetchRoles.rejected, (state) => {
        state.status = 'empty'
      })
  }
})

export const { clearRoles } = rolesSlice.actions

export default rolesSlice.reducer
