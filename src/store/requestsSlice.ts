import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import api from 'services/api'

export interface Request {
  id: number
  user_id: number
  type_id: number
  start_date: Date
  end_date: Date
  duration: number
  status: 'approved' | 'pending' | 'rejected'
  urgent: boolean
  description: string
  approver_description: string
  name: string
  days_left: number
  label: string
  cancellation: boolean
}

export interface RequestsState {
  requests: Request[]
  status: 'idle' | 'loading' | 'failed' | 'empty'
}
export const fetchRequests = createAsyncThunk<
  Request[],
  | {
      status?: string
      all?: boolean
      startDate?: Date | null
      endDate?: Date | null
      period?: string
      employee?: string
    }
  | undefined
>('requests/fetch', async (options) => {
  const { status, all, startDate, endDate, period, employee } = options ?? {}
  return await api.requests.getRequests(status, all, startDate, endDate, period, employee)
})

const initialState: RequestsState = {
  requests: [],
  status: 'empty'
}

export const requestsSlice = createSlice({
  name: 'requests',
  initialState,
  reducers: {
    clearRequests: (state) => {
      state.requests = []
      state.status = 'empty'
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRequests.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchRequests.fulfilled, (state, action) => {
        state.status = 'idle'
        state.requests = action.payload
      })
      .addCase(fetchRequests.rejected, (state) => {
        state.status = 'empty'
      })
  }
})

export const { clearRequests } = requestsSlice.actions

export default requestsSlice.reducer
