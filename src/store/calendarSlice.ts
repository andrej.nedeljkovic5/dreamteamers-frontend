import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import axios from 'services/api/apiCommon'
import { Request } from 'store/requestsSlice'

export interface UserTeams {
  user_id: number
  group_id: number
  isAdmin?: boolean
}

export interface NonWorkingDays {
  date: Date
}

export interface CalendarState {
  nonWorkingDates: NonWorkingDays[]
  userTeams: UserTeams[]
  requests: Request[]
  weekStart: 0 | 1
  status: 'empty' | 'idle' | 'loading' | 'failed'
}

export const initialState: CalendarState = {
  nonWorkingDates: [],
  userTeams: [],
  requests: [],
  weekStart: 0,
  status: 'empty'
}

export const fetchNonWorkingDays = createAsyncThunk<NonWorkingDays[]>('nonWorkingDays/fetch', async () => {
  const res = await axios.get(`non-working-days/`)
  return res
})

export const fetchRequests = createAsyncThunk<Request[], { all: boolean }>('requests/fetch', async (args) => {
  const res = await axios.get(`requests/?all=${String(args.all)}`)
  return res
})

export const CalendarSlice = createSlice({
  name: 'calendar',
  initialState,
  reducers: {
    setWeekStart: (state, action: PayloadAction<0 | 1>) => {
      state.weekStart = action.payload
      localStorage.setItem('weekStart', JSON.stringify(action.payload))
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchRequests.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchRequests.fulfilled, (state, action) => {
        state.status = 'idle'
        state.requests = action.payload
      })
      .addCase(fetchRequests.rejected, (state) => {
        state.status = 'failed'
      })
      .addCase(fetchNonWorkingDays.pending, (state) => {
        state.status = 'loading'
      })
      .addCase(fetchNonWorkingDays.fulfilled, (state, action) => {
        state.status = 'idle'
        state.nonWorkingDates = action.payload
      })
      .addCase(fetchNonWorkingDays.rejected, (state) => {
        state.status = 'failed'
      })
  }
})

export const { setWeekStart } = CalendarSlice.actions

export default CalendarSlice.reducer
