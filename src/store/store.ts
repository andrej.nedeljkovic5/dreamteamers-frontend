import { configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit'
import counterReducer from './counterSlice'
import authReducer from './authSlice'
import notificationReducer from './notifcationsSlice'
import teamsReducer from './teamsSlice'
import employeesReducer from './employeesSlice'
import rolesReducer from './roleSlice'
import calendarReducer from './calendarSlice'
import requestsReducer from './requestsSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
    notifications: notificationReducer,
    employees: employeesReducer,
    teams: teamsReducer,
    roles: rolesReducer,
    calendar: calendarReducer,
    requests: requestsReducer
  },
  middleware: getDefaultMiddleware({ serializableCheck: false })
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
// eslint-disable-next-line @typescript-eslint/no-invalid-void-type
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>
