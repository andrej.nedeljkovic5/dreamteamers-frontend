import React, { useEffect, useRef, useState } from 'react'
import { SelectChangeEvent } from '@mui/material/Select'
import Button from 'components/Button'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import { clearRequests, fetchRequests, Request } from 'store/requestsSlice'
import debounce from 'lodash/debounce'
import RequestTable from 'components/RequestTable'
import { AuthUser } from 'store/authSlice'
import Period from './Period'
import Statuses from './Statuses'
import Dates from './Dates'
const states = ['all', 'urgent', 'approved', 'pending', 'rejected']
const Requests: React.FC = () => {
  const dispatch = useAppDispatch()
  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)
  const allRequests = useAppSelector((state: RootState): Request[] => state.requests.requests)

  const [startDate, setStartDate] = React.useState<Date | undefined | null>(null)
  const [endDate, setEndDate] = React.useState<Date | undefined | null>(null)
  const [period, setPeriod] = React.useState('all')
  const [all, setAll] = useState(true)
  const [status, setStatus] = useState<string>()
  const searchInputRef = useRef<HTMLInputElement | null>(null)
  const [filteredRequests, setFilteredRequests] = useState<Request[]>(allRequests)
  const [isOpen, setIsOpen] = React.useState<HTMLDivElement | boolean>(false)

  useEffect(() => {
    setFilteredRequests(allRequests)
  }, [allRequests])

  useEffect(() => {
    void dispatch(fetchRequests({ status, all, startDate, endDate, period }))

    return () => {
      void dispatch(clearRequests())
    }
  }, [status, all, startDate, endDate, period])

  const handleFocus = (): void => {
    setIsOpen(!isOpen)
  }
  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const name = e.target.value.toLowerCase()
    setFilteredRequests(
      allRequests?.filter((request) => {
        return request.name.toLowerCase().includes(name)
      })
    )
  }
  const handleChangePeriod = (event: SelectChangeEvent<typeof period>): void => {
    setPeriod(event.target.value)
  }
  const resetFilter = (): void => {
    if (searchInputRef.current) {
      searchInputRef.current.value = ''
    }
    setStatus(undefined)
    setEndDate(null)
    setStartDate(null)
    setPeriod('all')
  }

  return (
    <div className="w-full max-w-7xl px-0 md:px-10">
      <div className="hidden md:block">
        <Statuses states={states} setStatus={setStatus} status={status} />
      </div>
      <div className="hidden flex-wrap md:block md:flex md:items-end md:gap-4">
        <div className="mr-auto">
          {user && (user.roles.includes('admin') || user.roles.includes('group_admin')) && (
            <input
              type="text"
              placeholder="Employee..."
              className="left-0 h-8 w-1/2 border-b-2 border-medium-gray bg-transparent px-1 text-sm text-font-main placeholder:text-font-main focus:outline-none dark:text-white dark:placeholder-white md:w-48"
              ref={searchInputRef}
              onChange={debounce(handleSearch, 400)}
            />
          )}
        </div>
        <Dates startDate={startDate} setStartDate={setStartDate} endDate={endDate} setEndDate={setEndDate}></Dates>
        <Period period={period} handleChangePeriod={handleChangePeriod} />
        <div className="flex">
          <Button onClick={resetFilter} className="btn-ghost">
            Clear all filters
          </Button>
        </div>
      </div>
      <div className="mx-3 mt-5 mb-10 flex items-end justify-between gap-3 md:mx-0 lg:mx-0 lg:justify-start lg:gap-16">
        <div className="w-full items-center">
          <div className="md:hidden">
            <input
              type="text"
              placeholder="Employee..."
              className="left-0 h-8 w-full border-b-2 border-medium-gray bg-transparent px-1 text-sm text-font-main placeholder:text-font-main focus:outline-none dark:text-white dark:placeholder-white"
              ref={searchInputRef}
              onChange={debounce(handleSearch, 400)}
            />
          </div>
          <div className="relative z-10 flex items-center justify-between md:hidden">
            <div className="collapse-arrow collapse w-full md:collapse-open">
              <input onMouseDown={handleFocus} type="checkbox" className="block  md:hidden" />
              <div className="collapse-title mr-auto block w-fit text-sm leading-7 md:hidden">
                {isOpen ? 'Hide filters' : 'Show filters'}
              </div>
              <div className="collapse-content w-full px-0 md:p-0">
                <div className="block flex flex-wrap items-end gap-1 text-sm">
                  <Statuses states={states} setStatus={setStatus} status={status} />
                </div>
                <Dates startDate={startDate} setStartDate={setStartDate} endDate={endDate} setEndDate={setEndDate} />
                <div className="mt-3 block flex flex-wrap items-end gap-2">
                  <Period period={period} handleChangePeriod={handleChangePeriod} />
                  <div className="mt-3 block flex items-end">
                    <Button onClick={resetFilter} className="btn-ghost">
                      Clear all filters
                    </Button>
                  </div>
                  <div className="form-control absolute top-1.5 right-0 z-20 inline-block w-24 md:hidden">
                    <label className="label cursor-pointer">
                      <span className="label-text">All</span>
                      <input
                        onChange={(newValue) => {
                          setAll((prev: any) => !prev)
                        }}
                        type="checkbox"
                        className="toggle toggle-primary"
                      />
                      <span className="label-text">Me</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <RequestTable filteredRequests={filteredRequests} user={user} all={all} setAll={setAll}></RequestTable>
        </div>
      </div>
    </div>
  )
}
export default Requests
