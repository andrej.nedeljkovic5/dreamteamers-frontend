import React, {SetStateAction} from 'react'
import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFns'
import {DatePicker} from '@mui/x-date-pickers/DatePicker'
import TextField from '@mui/material/TextField'

interface DatesProps {
  startDate: Date | null | undefined,
  setStartDate: (e: SetStateAction<Date | null | undefined>) => void,
  endDate: Date | null | undefined,
  setEndDate: (e: SetStateAction<Date | null | undefined>) => void
}

const Dates: React.FC<DatesProps> = ({ setStartDate, startDate, setEndDate, endDate }) => {
  return (
    <div className="mt-3 block flex flex-wrap items-end gap-2">
      <div>
        <label>Date</label>:
      </div>
      <div>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DatePicker
            onChange={(newValue) => {
              if (newValue !== null) {
                setStartDate(newValue)
              }
            }}
            renderInput={(params) => (
              <TextField
                variant="standard"
                sx={{ width: 95 }}
                size="small"
                {...params}
                className="dark:invert"
              />
            )}
            label="Start date "
            value={startDate}
          />
        </LocalizationProvider>
      </div>
      <div> - </div>
      <div>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DatePicker
            onChange={(newValue) => {
              if (newValue !== null) {
                setEndDate(newValue)
              }
            }}
            renderInput={(params) => (
              <TextField
                variant="standard"
                sx={{ width: 95 }}
                size="small"
                {...params}
                className="dark:invert"
              />
            )}
            label="End date"
            value={endDate}
          />
        </LocalizationProvider>
      </div>
    </div>
  )
}

export default Dates
