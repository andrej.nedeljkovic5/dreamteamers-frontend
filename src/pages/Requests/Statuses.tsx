import React, { SetStateAction } from 'react'
import cx from 'classnames'

interface StatusesProps {
  states: string[]
  setStatus: (e: SetStateAction<string | undefined>) => void
  status: string | undefined
}

const Statuses: React.FC<StatusesProps> = ({ states, setStatus, status }) => {
  return (
    <div className="mr-auto">
      <div className="mt-2 flex flex-wrap gap-2">
        <label>Show</label>:
        {states.map((state) => {
          return (
            <div
              key={state}
              className={"flex cursor-pointer flex-wrap after:content-['/'] last:after:content-['']"}
              onClick={(newValue) => {
                setStatus(state === 'all' ? undefined : state)
              }}
            >
              <div
                className={cx('cursor-pointer whitespace-pre capitalize', {
                  'text-blue': (!status && state === 'all') || state === status
                })}
              >
                {`${state} `}
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default Statuses
