import React from 'react'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Select, {SelectChangeEvent} from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'

interface PeriodProps {
  period: string,
  handleChangePeriod: (e: SelectChangeEvent) => void
}

const Period: React.FC<PeriodProps> = ({ period, handleChangePeriod }) => {
  return (
    <>
      <div>
        <label>Period</label>:
      </div>
      <div>
        <FormControl>
          <InputLabel className="dark:invert" id="demo-simple-select-label"></InputLabel>
          <Select
            variant="standard"
            sx={{ width: 100 }}
            className="dark:invert"
            name="role"
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={period}
            onChange={handleChangePeriod}
          >
            <MenuItem key="all" value="all">
              All
            </MenuItem>
            <MenuItem key="this_month" value="this_month">
              This month
            </MenuItem>
            <MenuItem key="this_year" value="this_year">
              This Year
            </MenuItem>
          </Select>
        </FormControl>
      </div>
    </>
  )
}

export default Period
