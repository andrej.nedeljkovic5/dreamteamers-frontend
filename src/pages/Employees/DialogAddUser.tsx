import React, { useEffect } from 'react'
import { useFormik } from 'formik'
import assign from 'lodash/assign'
import { object, string, InferType, date, array } from 'yup'
import { TextField, OutlinedInput, InputLabel, MenuItem, FormControl, Select, Chip, Box } from '@mui/material'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ValidationMessage } from 'services/api/apiCommon'
import { RootState, store } from 'store/store'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import {
  resetSaveUserStatus,
  saveUser,
  clearApiError,
  clearValidationErrors,
  fetchEmployees
} from 'store/employeesSlice'
import { Team } from 'store/teamsSlice'
import { Role } from 'store/roleSlice'
import Button from 'components/Button/Button'
import Dialog from 'components/Dialog'
import TextInput from 'components/TextInput/TextInput'
import ErrorText from 'components/ErrorText/ErrorText'
import { ReactComponent as Edit } from 'assets/icons/edit.svg'
import { ReactComponent as Umbrella } from 'assets/icons/vacation.svg'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
}

const addUserSchema = object({
  firstname: string().required('Required field').default(''),
  lastname: string().required('Required field').default(''),
  email: string().email('Invalid email').required('Required field').default(''),
  role_id: string().required('Required field').default(''),
  job_title: string().optional().default(''),
  days_left: string().required('Required field').default(''),
  employed_since: date().optional(),
  teams: array(string().required()).optional().default([])
})

type FormValues = InferType<typeof addUserSchema>
interface DialogProps {
  dialogOpen: boolean
  toggleDialogOpen: () => void
}

const DialogAddUser: React.FC<DialogProps> = (props: DialogProps) => {
  const formik = useFormik({
    initialValues: addUserSchema.getDefault(),
    validationSchema: addUserSchema,
    onSubmit: async (values: FormValues) => {
      const {
        firstname,
        lastname,
        email,
        role_id: roleInit,
        job_title: position,
        days_left: freeDays,
        teams,
        employed_since: employedSince
      } = values

      await dispatch(
        saveUser({
          name: `${firstname} ${lastname}`,
          email,
          roleInit,
          position,
          freeDays,
          selectedTeams: teams,
          employedSince: employedSince ?? null
        })
      )
    }
  })

  const teams = useAppSelector((state: RootState): Team[] => state.teams.teams)
  const roles = useAppSelector((state: RootState): Role[] => state.roles.roles)
  const saveUserStatus = useAppSelector((state: RootState): string => state.employees.saveUserStatus)
  const validationErrors = useAppSelector((state: RootState): ValidationMessage[] => state.employees.validationErrors)
  const apiError = useAppSelector((state: RootState): string => state.employees.apiError)

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (validationErrors?.length) {
      formik.setErrors(
        assign(
          {},
          ...validationErrors.map((err) =>
            err.type === 'name'
              ? {
                  firstname: err.message,
                  lastname: err.message
                }
              : {
                  [err.type]: err.message
                }
          )
        )
      )
    }
  }, [validationErrors])

  useEffect(() => {
    if (saveUserStatus === 'idle') {
      props.toggleDialogOpen()
      showToastMessage()
      void dispatch(resetSaveUserStatus())
    }
  }, [saveUserStatus])

  const showToastMessage = (): void => {
    toast.success('User successfully added!', {
      position: toast.POSITION.TOP_CENTER
    })
  }
  return (
    <div className="flex h-full w-full items-center justify-end gap-y-7 md:w-fit lg:justify-start">
      {
        <Dialog
          title={'Add new Employee'}
          onClose={() => {
            void dispatch(clearApiError())
            void dispatch(clearValidationErrors())
            props.toggleDialogOpen()
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <div className="display-grid m-1 flex flex-col gap-y-3">
              <div className="display-grid flex flex-col">
                <div className="inline-flex">
                  <Edit className="relative left-1 h-10 w-10  pr-1 dark:invert md:-left-2"></Edit>
                  <label className="py-2.5">Personal information</label>
                </div>
                <TextInput
                  name="firstname"
                  id="firstname"
                  value={formik.values.firstname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  placeholder="First name*"
                  error={formik.touched.firstname && !!formik.errors.firstname}
                  helperText={formik.touched.firstname && formik.errors.firstname}
                />
                <TextInput
                  name="lastname"
                  id="lastname"
                  value={formik.values.lastname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  placeholder={'Last name*'}
                  error={formik.touched.lastname && !!formik.errors.lastname}
                  helperText={formik.touched.lastname && formik.errors.lastname}
                />
                <TextInput
                  name="email"
                  id="email"
                  type="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  placeholder={'Email*'}
                  error={formik.touched.email && !!formik.errors.email}
                  helperText={formik.touched.email && formik.errors.email}
                />
              </div>
              <div className="flex flex-col gap-y-2">
                <div className="inline-flex">
                  <Edit className="relative left-1 h-10 w-10 pr-1 dark:invert md:-left-2"></Edit>
                  <label className="py-2.5">Job information</label>
                </div>
                <FormControl size="small" fullWidth>
                  <InputLabel className="dark:invert" id="userRole-label">
                    Role
                  </InputLabel>
                  <Select
                    name="role_id"
                    id="role_id"
                    labelId="userRole-label"
                    label="Role"
                    className="dark:invert"
                    value={formik.values.role_id}
                    onChange={formik.handleChange}
                    error={formik.touched.role_id && !!formik.errors.role_id}
                  >
                    {roles.map((role) => (
                      <MenuItem key={role.id} value={role.id}>
                        {role.name}
                      </MenuItem>
                    ))}
                  </Select>
                  <ErrorText>{formik.touched.role_id && formik.errors.role_id}</ErrorText>
                </FormControl>
                <TextInput
                  name="job_title"
                  id="job_title"
                  value={formik.values.job_title}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  placeholder="Position"
                  error={formik.touched.job_title && !!formik.errors.job_title}
                  helperText={formik.touched.job_title && formik.errors.job_title}
                />
                <div className="flex items-center gap-4">
                  <div>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                      <DatePicker
                        InputAdornmentProps={{ position: 'start' }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            size="small"
                            className="dark:invert"
                            error={formik.touched.employed_since && !!formik.errors.employed_since}
                            // Helper text currently doesnt work, issue with react / formik types versions, check the link for more info
                            // https://github.com/jaredpalmer/formik/issues/3683
                            // helperText={formik.touched.employed_since && formik.errors.employed_since}
                          />
                        )}
                        label="Employed since:"
                        value={formik.values.employed_since}
                        onChange={(value) => {
                          void formik.setFieldValue('employed_since', value, true)
                        }}
                      />
                    </LocalizationProvider>
                    <ErrorText>{formik.touched.employed_since && formik.errors.employed_since}</ErrorText>
                  </div>
                  <div className="mb-1 flex w-1/2 items-start justify-end gap-1">
                    <Umbrella className="h-10 w-10 pr-1 dark:invert"></Umbrella>
                    <TextInput
                      name="days_left"
                      id="days_left"
                      type="number"
                      value={formik.values.days_left}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      placeholder={'Free days*'}
                      error={formik.touched.days_left && !!formik.errors.days_left}
                      helperText={formik.touched.days_left && formik.errors.days_left}
                    />
                  </div>
                </div>
                <FormControl size="small" fullWidth>
                  <InputLabel className="dark:invert" id="teams-label">
                    Teams
                  </InputLabel>
                  <Select
                    name="teams"
                    id="teams"
                    labelId="teams-label"
                    label="Teams"
                    className="dark:invert"
                    value={formik.values.teams}
                    onChange={formik.handleChange}
                    error={formik.touched.teams && !!formik.errors.teams}
                    multiple
                    input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                    renderValue={(selected) => (
                      <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                        {selected.map((teamId) => {
                          const selectedTeam = teams.find((t) => t.id === +teamId)
                          return <Chip key={teamId} label={selectedTeam?.name} />
                        })}
                      </Box>
                    )}
                    MenuProps={MenuProps}
                  >
                    {teams.length &&
                      teams.map((team) => (
                        <MenuItem key={team.id} value={team.id}>
                          {team.name}
                        </MenuItem>
                      ))}
                  </Select>
                  <ErrorText>{formik.touched.teams && formik.errors.teams}</ErrorText>
                </FormControl>
              </div>
              <div className="ml-auto">
                <div>
                  <ErrorText>{apiError}</ErrorText>
                </div>
                <label
                  htmlFor="dialogBox"
                  className="btn btn-ghost btn-sm rounded-sm text-sm font-semibold leading-5"
                  onChange={() => props.toggleDialogOpen}
                >
                  Close
                </label>
                <Button type="submit" className={formik.isSubmitting ? 'loading' : ''}>
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Dialog>
      }
    </div>
  )
}

export default DialogAddUser
