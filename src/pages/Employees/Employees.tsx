import React, { useEffect, useRef, useState } from 'react'
import debounce from 'lodash/debounce'
import { RootState } from 'store/store'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { clearEmployees, EmployeeUser, fetchEmployees } from 'store/employeesSlice'
import { clearTeams, fetchTeams, Team } from 'store/teamsSlice'
import { clearRoles, fetchRoles } from 'store/roleSlice'
import Employee from 'components/Employee/Employee'
import Button from 'components/Button/Button'
import { ReactComponent as AddIcon } from 'assets/icons/add.svg'
import LoadingCard from 'components/Employee/LoadingCard'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import DialogAddUser from 'pages/Employees/DialogAddUser'

const Employees: React.FC = () => {
  const [dialogOpen, setDialogOpen] = useState(false)

  const toggleDialogOpen = (): void => {
    setDialogOpen(!dialogOpen)
  }

  const allEmployees = useAppSelector((state: RootState): EmployeeUser[] => state.employees.employees)
  const employeesStatus = useAppSelector((state: RootState): string => state.employees.fetchEmployeesStatus)
  const [filteredEmployees, setFilteredEmployees] = useState<EmployeeUser[]>(allEmployees)
  const searchInputRef = useRef<HTMLInputElement | null>(null)
  const teamFilterRef = useRef<HTMLSelectElement | null>(null)

  const dispatch = useAppDispatch()

  useEffect(() => {
    void dispatch(fetchEmployees())
    void dispatch(fetchTeams())
    void dispatch(fetchRoles())

    return () => {
      void dispatch(clearTeams())
      void dispatch(clearRoles())
      void dispatch(clearEmployees())
    }
  }, [])

  useEffect(() => {
    setFilteredEmployees(allEmployees)
  }, [allEmployees])

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const name = e.target.value.toLowerCase()
    setFilteredEmployees(
      allEmployees?.filter((emp) => {
        return emp.name.toLowerCase().includes(name)
      })
    )
  }
  const handleFilterByTeam = (e: SelectChangeEvent<typeof teamUser>): void => {
    void dispatch(fetchEmployees(e.target.value === '' ? undefined : +e.target.value))
  }
  const [teamUser, setTeamUser] = React.useState('')
  const teams = useAppSelector((state: RootState): Team[] => state.teams.teams)

  const resetFilter = (): void => {
    void dispatch(fetchEmployees())
    if (teamFilterRef.current) {
      teamFilterRef.current.value = ''
    }
    if (searchInputRef.current) {
      searchInputRef.current.value = ''
    }
  }
  return (
    <>
      {dialogOpen && <DialogAddUser dialogOpen={dialogOpen} toggleDialogOpen={toggleDialogOpen} />}
      <ToastContainer theme="colored" hideProgressBar></ToastContainer>
      <div className="w-full max-w-7xl px-0 md:px-20">
        <div className="mx-3 mb-10 mt-10 flex flex-col items-end justify-between gap-3 md:mx-0 md:mb-16 md:flex-row md:gap-7">
          <input
            type="text"
            placeholder="Search..."
            className="h-8 w-full border-b-2 border-medium-gray bg-transparent px-1 text-sm text-font-main placeholder:text-font-main focus:outline-none dark:text-white dark:placeholder-white md:w-48"
            ref={searchInputRef}
            onChange={debounce(handleSearch, 400)}
          />
          <div className="flex w-full items-center justify-end gap-7 md:w-fit lg:justify-start">
            <div className="collapse-arrow collapse w-full md:collapse-open">
              <input type="checkbox" className="block md:hidden" />
              <div className="collapse-title ml-auto block w-fit text-sm leading-7 md:hidden">Show filters</div>
              <div className="collapse-content px-0 md:p-0">
                <FormControl size="small" fullWidth>
                  <InputLabel className="dark:invert" id="demo-simple-select-label">
                    All teams
                  </InputLabel>
                  <Select
                    className=" dark:invert md:w-48"
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={teamUser}
                    label="Team"
                    onChange={handleFilterByTeam}
                  >
                    {teams.length &&
                      teams.map((team) => (
                        <MenuItem key={team.id} value={team.id}>
                          {team.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </div>
            </div>
            <Button className="hidden normal-case md:block" onClick={() => setDialogOpen((prev) => !prev)}>
              Add New Employee
            </Button>
          </div>
          <button
            onClick={() => setDialogOpen((prev) => !prev)}
            className="btn btn-primary btn-circle fixed bottom-10 right-8 z-50 h-[50px] w-[50px] text-3xl normal-case dark:btn-secondary md:hidden"
          >
            <AddIcon className="w-5" stroke="white" strokeWidth={1} fill="white" />
          </button>
        </div>
        <div className="mb-5 flex flex-wrap items-start justify-center gap-8 lg:justify-start lg:gap-16">
          {employeesStatus === 'idle' ? (
            filteredEmployees.length ? (
              filteredEmployees.map((employee) => <Employee key={employee.id} employee={employee} />)
            ) : (
              <div className="flex w-full flex-col items-center gap-3">
                <div className="text-center text-xl font-bold">No employees found matching your criteria</div>
                <div>Please try filtering by more general criteria.</div>
                <button className="btn btn-ghost mt-10" onClick={resetFilter}>
                  Clear all filters
                </button>
              </div>
            )
          ) : (
            Array.from(Array(15)).map((val, index) => <LoadingCard key={index} />)
          )}
        </div>
      </div>
    </>
  )
}

export default Employees
