import React, { useEffect, useRef, useState } from 'react'
import debounce from 'lodash/debounce'
import { RootState } from 'store/store'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { clearTeams, fetchTeams, Team } from 'store/teamsSlice'
import Button from 'components/Button/Button'
import TeamRow from 'components/TeamRow/TeamRow'
import TeamCard from 'components/TeamCard/TeamCard'
import LoadingTeamRow from 'components/TeamRow/LoadingTeamRow'
import LoadingTeamCard from 'components/TeamCard/LoadingTeamCard'
const AllTeams: React.FC = () => {
  const allTeams = useAppSelector((state: RootState): Team[] => state.teams.teams)
  const teamsStatus = useAppSelector((state: RootState): string => state.teams.status)

  const searchInputRef = useRef<HTMLInputElement | null>(null)

  const [filteredTeams, setFilteredTeams] = useState<Team[]>(allTeams)

  const dispatch = useAppDispatch()

  useEffect(() => {
    void dispatch(fetchTeams(true))

    return () => {
      void dispatch(clearTeams())
    }
  }, [])

  useEffect(() => {
    setFilteredTeams(allTeams)
  }, [allTeams])

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const name = e.target.value.toLowerCase()
    setFilteredTeams(
      allTeams?.filter((team) => {
        return team.name.toLowerCase().includes(name)
      })
    )
  }

  const resetFilter = (): void => {
    setFilteredTeams(allTeams)
    if (searchInputRef.current) {
      searchInputRef.current.value = ''
    }
  }

  return (
    <div className="w-full max-w-7xl px-0 md:px-10">
      <div className="mx-3 mb-10 mt-10 flex items-end justify-between gap-3 md:mx-0 md:mb-16 md:flex-row md:gap-7">
        <input
          type="text"
          placeholder="Team..."
          className="left-0 h-8 w-1/2 border-b-2 border-medium-gray bg-transparent px-1 text-sm text-font-main placeholder:text-font-main focus:outline-none dark:text-white dark:placeholder-white md:w-48"
          ref={searchInputRef}
          onChange={debounce(handleSearch, 400)}
        />
        <div className="flex w-full items-center justify-end gap-7 md:w-fit lg:justify-start">
          <Button className="normal-case md:block">Make New Team</Button>
        </div>
      </div>
      <div className="mx-3 flex flex-wrap items-start justify-center gap-8 md:mx-0 lg:mx-0 lg:justify-start lg:gap-16">
        {filteredTeams.length || teamsStatus !== 'idle' ? (
          <div className="w-full">
            <div className="w-full bg-blue p-1 text-center text-white dark:bg-blue-dark">All teams</div>
            <div className="w-full overflow-x-auto">
              <table className="hidden md:table-compact md:table md:w-full">
                <thead>
                  <tr>
                    <th className="bg-amber-700 normal-case">Team Name</th>
                    <th>Team Admin</th>
                    <th>Team Members</th>
                    <th>About Team</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody className="border">
                  {teamsStatus === 'idle'
                    ? filteredTeams.map((team) => <TeamRow key={team.id} team={team} />)
                    : Array.from(Array(4)).map((val, index) => <LoadingTeamRow key={index} />)}
                </tbody>
              </table>
            </div>
            <div className="flex-col divide-y md:hidden">
              {teamsStatus === 'idle'
                ? filteredTeams.map((team) => <TeamCard key={team.id} team={team} />)
                : Array.from(Array(4)).map((val, index) => <LoadingTeamCard key={index} />)}
            </div>
          </div>
        ) : (
          <div className="flex w-full flex-col items-center gap-3">
            <div className="text-center text-xl font-bold">No teams found matching your criteria</div>
            <div>Please try filtering by more general criteria.</div>
            <button className="btn btn-ghost mt-10" onClick={resetFilter}>
              Clear all filters
            </button>
          </div>
        )}
      </div>
    </div>
  )
}

export default AllTeams
