import React, { ReactElement, ReactNode, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import assign from 'lodash/assign'
import { object, string, InferType } from 'yup'
import { ValidationMessage } from 'services/api/apiCommon'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { login } from 'store/authSlice'
import { RootState } from 'store/store'
import TextInput from 'components/TextInput/TextInput'
import ErrorText from 'components/ErrorText/ErrorText'
import Button from 'components/Button/Button'

interface LoginProps {
  children?: ReactNode
}

const loginSchema = object({
  email: string().email('Invalid email').required('Required field').default(''),
  password: string().min(5, 'Password should be at least 5 characters long').required('Required field').default('')
})

type FormValues = InferType<typeof loginSchema>

const Login: React.FC<LoginProps> = ({ children }): ReactElement => {
  const validationErrors = useAppSelector((state: RootState): ValidationMessage[] => state.auth.validationErrors)
  const apiError = useAppSelector((state: RootState): string => state.auth.apiError)

  const dispatch = useAppDispatch()

  const formik = useFormik({
    initialValues: loginSchema.getDefault(),
    validationSchema: loginSchema,
    onSubmit: async (values: FormValues) => await dispatch(login(values))
  })

  useEffect(() => {
    if (validationErrors?.length) {
      formik.setErrors(assign({}, ...validationErrors.map((err) => ({ [err.type]: err.message }))))
    }
  }, [validationErrors])

  return (
    <form className="flex flex-col gap-7" onSubmit={formik.handleSubmit}>
      <div className="text-center">Sign in</div>
      <div className="text-sm tracking-[-0.18px]">
        Hi, nice to see you again! Do you have some new vacation plans? Let&apos;s start!
      </div>
      <div className="flex flex-col">
        <div>
          <TextInput
            id="email"
            name="email"
            placeholder="Email"
            type="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            error={formik.touched.email && !!formik.errors.email}
            helperText={formik.touched.email && formik.errors.email}
          />
        </div>
        <div>
          <TextInput
            id="password"
            name="password"
            placeholder="Password"
            type="password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.password}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <Link to="/auth/forgot-password" className="block text-sm text-blue hover:underline">
            I forgot my password
          </Link>
        </div>
      </div>
      <div>
        <ErrorText>{apiError}</ErrorText>
        <div className="flex justify-end gap-2.5">
          <Button type="submit" className={formik.isSubmitting ? 'loading' : ''}>
            Sign in
          </Button>
        </div>
      </div>
    </form>
  )
}

export default Login
