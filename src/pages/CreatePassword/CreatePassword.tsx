import React, { useEffect } from 'react'
import TextInput from 'components/TextInput/TextInput'
import { Link } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { findUser, updatePassword, login } from 'store/authSlice'
import Button from 'components/Button'
import Loader from 'components/Loader/Loader'
import { RootState } from 'store/store'
import { ValidationMessage } from 'services/api/apiCommon'
import { useFormik } from 'formik'
import { InferType, object, string, ref } from 'yup'
import assign from 'lodash/assign'
import ErrorText from 'components/ErrorText/ErrorText'

const createPasswordSchema = object({
  password: string().min(5, 'Password should be at least 5 characters long').required('Required field').default(''),
  repeatPassword: string()
    .required('Required field')
    .default('')
    .oneOf([ref('password')], 'Passwords must match!')
})

type FormValues = InferType<typeof createPasswordSchema>
const CreatePassword: React.FC = () => {
  const userExistsStatus = useAppSelector((state: RootState): string => state.auth.userExistsStatus)
  const validationErrors = useAppSelector((state: RootState): ValidationMessage[] => state.auth.validationErrors)
  const apiError = useAppSelector((state: RootState): string => state.auth.apiError)

  const searchParams = new URLSearchParams(window.location.search)
  const email = searchParams.get('email') ?? ''
  const token = searchParams.get('token') ?? ''

  const dispatch = useAppDispatch()

  const formik = useFormik({
    initialValues: createPasswordSchema.getDefault(),
    validationSchema: createPasswordSchema,
    onSubmit: async (values: FormValues) => {
      await dispatch(updatePassword({ email, password: values.password })).then(() => {
        void dispatch(login({ email, password: values.password }))
      })
    }
  })
  useEffect(() => {
    if (validationErrors?.length) {
      formik.setErrors(assign({}, ...validationErrors.map((err) => ({ [err.type]: err.message }))))
    }
  }, [validationErrors])

  useEffect(() => {
    if (email && token) {
      void dispatch(findUser({ email, token }))
    }
  }, [email, token])

  return userExistsStatus === 'loading' || userExistsStatus === 'empty' ? (
    <Loader />
  ) : (
    <>
      {userExistsStatus === 'idle' ? (
        <form onSubmit={formik.handleSubmit} className="flex flex-col gap-7">
          <div className="text-center">Create Password</div>
          <div className="text-sm tracking-[-0.18px]">
            Welcome to DEVersity Vacation tracker! Add your password and start making plans :)
          </div>
          <div className="flex flex-col gap-1">
            <TextInput value={email} name="email" placeholder="Email" type="email" disabled={true} />
            <TextInput
              name="password"
              placeholder="Password"
              type="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.password}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.password && formik.errors.password}
            />
            <TextInput
              name="repeatPassword"
              placeholder="Repeat password"
              type="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.repeatPassword}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.repeatPassword && formik.errors.repeatPassword}
            />
          </div>
          <div>
            <ErrorText>{apiError}</ErrorText>
            <div className="flex justify-end gap-2.5">
              <Button type="submit" className={formik.isSubmitting ? 'loading' : ''}>
                Save
              </Button>
            </div>
            <Link to="/auth/login" className="block pt-6 text-center text-sm text-blue hover:underline">
              Already have a password? Log in!
            </Link>
          </div>
        </form>
      ) : (
        <div className="text-center text-red">Something went wrong, please contact your admin :)</div>
      )}
    </>
  )
}

export default CreatePassword
