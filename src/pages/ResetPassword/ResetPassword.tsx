import React, { ReactElement, SyntheticEvent, useEffect, useRef, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { resetPassword } from 'store/authSlice'
import { RootState } from 'store/store'
import { ValidationMessage } from 'services/api/apiCommon'
import TextInput from 'components/TextInput/TextInput'
import Button from 'components/Button/Button'
import { InferType, object, string, ref } from 'yup'
import { useFormik } from 'formik'
import assign from 'lodash/assign'
import ErrorText from 'components/ErrorText/ErrorText'

const resetPasswordSchema = object({
  password: string().min(5, 'Password should be at least 5 characters long').required('Required field').default(''),
  repeatPassword: string()
    .required('Required field')
    .default('')
    .oneOf([ref('password')], 'Passwords must match!')
})
type FormValues = InferType<typeof resetPasswordSchema>

const ResetPassword: React.FC = (): ReactElement => {
  const validationErrors = useAppSelector((state: RootState): ValidationMessage[] => state.auth.validationErrors)
  const apiError = useAppSelector((state: RootState): string => state.auth.apiError)
  const passwordChanged = useAppSelector((state: RootState): boolean => state.auth.passwordChanged)

  const navigate = useNavigate()

  const searchParams = new URLSearchParams(window.location.search)
  const email = searchParams.get('email') ?? ''
  const token = searchParams.get('token') ?? ''

  const dispatch = useAppDispatch()

  const formik = useFormik({
    initialValues: resetPasswordSchema.getDefault(),
    validationSchema: resetPasswordSchema,
    onSubmit: async (values: FormValues) => {
      void dispatch(resetPassword({ password: values.password, email, token }))
    }
  })
  useEffect(() => {
    if (validationErrors?.length) {
      formik.setErrors(assign({}, ...validationErrors.map((err) => ({ [err.type]: err.message }))))
    }
  }, [validationErrors])

  return (
    <form onSubmit={formik.handleSubmit} className="flex flex-col gap-7">
      {<div className="text-center">{apiError ? 'Failure!' : passwordChanged ? 'Success!' : 'Reset password'}</div>}
      {
        <div className="text-center tracking-[-0.18px]">
          {apiError !== null
            ? apiError
            : passwordChanged
            ? 'Your password is now reset, you can log in with your new password!'
            : 'Create your new password!'}
        </div>
      }
      {!apiError && !passwordChanged && (
        <div className="flex flex-col">
          <div>
            <TextInput
              name="password"
              placeholder="Password"
              type="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.password}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.password && formik.errors.password}
            />
          </div>

          <div>
            <TextInput
              name="repeatPassword"
              placeholder="Repeat password"
              type="password"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.repeatPassword}
              error={formik.touched.repeatPassword && !!formik.errors.repeatPassword}
              helperText={formik.touched.repeatPassword && formik.errors.repeatPassword}
            />
          </div>
        </div>
      )}
      <div>
        <ErrorText>{apiError}</ErrorText>
        {!apiError && !passwordChanged && (
          <div className="flex justify-end gap-2.5">
            <Button type="button" onClick={() => navigate('/auth/login')} variant="transparent">
              Cancel
            </Button>
            <Button type="submit" className={formik.isSubmitting ? 'loading' : ''}>
              RESET
            </Button>
          </div>
        )}
        {passwordChanged && (
          <div className="flex justify-center gap-2.5">
            <Button type="button" onClick={() => navigate('/auth/login')}>
              LOG IN
            </Button>
          </div>
        )}
      </div>
    </form>
  )
}

export default ResetPassword
