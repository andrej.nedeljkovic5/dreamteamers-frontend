import React, { ReactElement } from 'react'
import Calendar from 'components/Calendar/Calendar'

const CalendarPage: React.FC = (): ReactElement => {
  return (
    <div className="w-full 3xl:w-[1600px]">
      <Calendar />
    </div>
  )
}

export default CalendarPage
