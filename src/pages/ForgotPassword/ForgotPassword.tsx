import React, { ReactElement, ReactNode, SyntheticEvent, useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { forgotPassword, cleanErrors } from 'store/authSlice'
import { RootState } from 'store/store'
import { ValidationMessage } from 'services/api/apiCommon'
import TextInput from 'components/TextInput/TextInput'
import Button from 'components/Button'

interface ForgotPasswordProps {
  children?: ReactNode
}
const ForgotPassword: React.FC<ForgotPasswordProps> = ({ children }): ReactElement => {
  const validationErrors = useAppSelector((state: RootState): ValidationMessage[] => state.auth.validationErrors)
  const apiError = useAppSelector((state: RootState): string => state.auth.apiError)
  const status = useAppSelector((state: RootState): string => state.auth.status)
  const emailSent = useAppSelector((state: RootState): boolean => state.auth.emailSent)

  const [email, setEmail] = useState<string>('')

  const navigate = useNavigate()

  const emailErrorRef = useRef<HTMLDivElement | null>(null)
  const credentialsErrorRef = useRef<HTMLDivElement | null>(null)

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (emailErrorRef.current !== null && credentialsErrorRef.current !== null) {
      emailErrorRef.current.innerText = ' '
      credentialsErrorRef.current.innerText = ' '
    }
    validationErrors?.forEach((error: ValidationMessage) => {
      if (error.type === 'email' && emailErrorRef.current) {
        emailErrorRef.current.innerText = error.message
      }
      if (error.type === 'credentials' && credentialsErrorRef.current) {
        credentialsErrorRef.current.innerText = error.message
      }
    })
    if (apiError && emailErrorRef.current) {
      emailErrorRef.current.innerText = apiError
    }
  }, [validationErrors, apiError])

  const handleSubmit = (e: SyntheticEvent): void => {
    e.preventDefault()
    void dispatch(
      forgotPassword({
        email
      })
    )
  }

  const content = emailSent
    ? 'Please check your email! We have sent you a link to create your account!'
    : "Forgot your password? Don't worry just give us your email and we will send you a reset password link!"

  return (
    <form onSubmit={handleSubmit} className="flex flex-col gap-7">
      <div className="text-center">Reset password</div>
      <div className="text-center text-sm tracking-[-0.18px]">{content}</div>
      <div className="flex flex-col">
        <div>
          {!emailSent && (
            <TextInput
              value={email}
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Email"
              type="email"
            />
          )}
          {!emailSent && <div ref={emailErrorRef} className="min-h-8 text-end text-xs text-red lg:text-sm"></div>}
        </div>
      </div>
      {!emailSent && (
        <div>
          <div ref={credentialsErrorRef} className="min-h-4 pb-1 text-end text-xs text-red lg:text-sm"></div>
          <div className="flex justify-end gap-2.5">
            <Button
              type="button"
              onClick={() => {
                dispatch(cleanErrors())
                navigate('/auth/login')
              }}
              variant="transparent"
            >
              Cancel
            </Button>

            <Button type="submit" className={status ?? 'loading'}>
              SEND
            </Button>
          </div>
        </div>
      )}
    </form>
  )
}

export default ForgotPassword
