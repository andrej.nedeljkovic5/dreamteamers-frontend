import React, { useEffect, useMemo } from 'react'
import { isThisYear, isPast, isBefore, getYear, isFuture, differenceInDays, format } from 'date-fns'
import cx from 'classnames'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { RootState } from 'store/store'
import { clearRequests, fetchRequests, Request } from 'store/requestsSlice'
import { AuthUser, fetchDashboardData } from 'store/authSlice'
import RequestTable from 'components/RequestTable'
import Employee from 'components/Employee'
import LineWithDots from 'components/LineWithDots/LineWithDots'
import Button from 'components/Button'
import { ReactComponent as Edit } from 'assets/icons/edit.svg'

const Dashboard: React.FC = () => {
  const dispatch = useAppDispatch()

  const user = useAppSelector((state: RootState): AuthUser | null => state.auth.user)
  const allRequests = useAppSelector((state: RootState): Request[] => state.requests.requests)

  const {
    thisYearRequests,
    thisYearFinishedRequests,
    lastYearRequests,
    lastYearFinishedRequests,
    daysUsed,
    finishedRequestsDiff,
    daysApproved,
    daysPending,
    pendingRequestsCount,
    upcomingApprovedRequest,
    upcomingPendingRequest,
    daysLeftForResponse
  } = useMemo(() => {
    const thisYearRequests = allRequests
      .filter((req) => isThisYear(req.end_date))
      .sort((a, b) => (isBefore(a.start_date, b.start_date) ? -1 : 1))
    const thisYearFinishedRequests = thisYearRequests.filter(
      (req) =>
        isPast(req.end_date) &&
        ((!req.cancellation && ['pending', 'approved'].includes(req.status)) ||
          (req.cancellation && req.status === 'rejected'))
    )
    const thisYearUpcomingRequests = thisYearRequests.filter((req) => isFuture(req.start_date))

    const lastYearRequests = allRequests
      .filter(
        (req) =>
          getYear(req.end_date) === getYear(new Date()) - 1 &&
          ((!req.cancellation && ['pending', 'approved'].includes(req.status)) ||
            (req.cancellation && req.status === 'rejected'))
      )
      .sort((a, b) => (isBefore(a.start_date, b.start_date) ? -1 : 1))
    const lastYearFinishedRequests = lastYearRequests.filter((req) => isPast(req.end_date) && req.status === 'approved')

    const daysUsed = thisYearFinishedRequests
      .filter(
        (req) => (!req.cancellation && req.status === 'approved') || (req.cancellation && req.status === 'rejected')
      )
      .reduce((a, b) => a + b.duration, 0)

    const finishedRequestsDiff =
      lastYearFinishedRequests.reduce((a, b) => a + b.duration, 0) -
      thisYearFinishedRequests.reduce((a, b) => a + b.duration, 0)

    const approvedRequests = thisYearUpcomingRequests.filter(
      (req) => (!req.cancellation && req.status === 'approved') || (req.cancellation && req.status === 'rejected')
    )
    const daysApproved = approvedRequests.reduce((a, b) => a + b.duration, 0)

    const pendingRequests = thisYearUpcomingRequests.filter((req) => req.status === 'pending')
    const daysPending = pendingRequests.reduce((a, b) => a + b.duration, 0)
    const pendingRequestsCount = pendingRequests.length

    const upcomingApprovedRequest = approvedRequests[0]
    const upcomingPendingRequest = pendingRequests.at(pendingRequests.length - 1)
    const daysLeftForResponse = upcomingPendingRequest
      ? differenceInDays(upcomingPendingRequest.start_date, new Date())
      : 0

    return {
      thisYearRequests,
      thisYearFinishedRequests,
      lastYearRequests,
      lastYearFinishedRequests,
      daysUsed,
      finishedRequestsDiff,
      daysApproved,
      daysPending,
      pendingRequestsCount,
      upcomingApprovedRequest,
      upcomingPendingRequest,
      daysLeftForResponse
    }
  }, [allRequests])

  useEffect(() => {
    void dispatch(fetchDashboardData())
    void dispatch(fetchRequests())

    return () => {
      void dispatch(clearRequests())
    }
  }, [])

  return (
    <div className="mt-7 flex w-full max-w-7xl flex-col gap-16 px-0 md:px-10">
      <div className="mx-4 flex flex-col items-center gap-16 md:mx-0 lg:gap-0 xl:flex-row">
        {user && (
          <div className="flex flex-col items-center">
            <Employee employee={user} dashboardVersion />
            <Edit
              className="inline w-8 hover:cursor-pointer dark:invert"
              onClick={() => {
                console.log('Edit')
              }}
            />
            <Button className="mt-5 md:hidden">View Requests</Button>
          </div>
        )}
        <div className="stats stats-vertical w-full border text-center shadow dark:text-very-light-gray lg:stats-horizontal lg:h-32 lg:text-left">
          <div className="stat">
            <div className="stat-title">Days Used</div>
            <div
              className={cx('stat-value', {
                'text-blue': daysUsed > 0,
                'text-medium-gray dark:text-medium-gray-2': daysUsed <= 0
              })}
            >
              {daysUsed}
            </div>
            <div className="stat-desc">
              {Math.sign(finishedRequestsDiff) === 1 ? '↘︎' : '↗︎'} {Math.abs(finishedRequestsDiff)} from last year
            </div>
          </div>

          <div className="stat">
            <div className="stat-title">Days Approved</div>
            <div
              className={cx('stat-value', {
                'text-blue': daysApproved > 0,
                'text-medium-gray dark:text-medium-gray-2': daysApproved <= 0
              })}
            >
              {daysApproved}
            </div>
            <div className="stat-desc">
              {upcomingApprovedRequest ? (
                <>
                  Upcoming{' '}
                  {new Intl.DateTimeFormat('en-US', {
                    month: 'short',
                    day: 'numeric'
                  }).format(upcomingApprovedRequest.start_date)}{' '}
                  to{' '}
                  {new Intl.DateTimeFormat('en-US', {
                    month: 'short',
                    day: 'numeric'
                  }).format(upcomingApprovedRequest.end_date)}
                </>
              ) : (
                'No upcoming vacation'
              )}
            </div>
          </div>

          <div className="stat">
            <div className="stat-title">Days Pending</div>
            <div
              className={cx('stat-value', {
                'text-blue': daysPending > 0,
                'text-medium-gray dark:text-medium-gray-2': daysPending <= 0
              })}
            >
              {daysPending}
            </div>
            <div className="stat-desc">
              {upcomingPendingRequest ? (
                <>
                  Upcoming{' '}
                  {new Intl.DateTimeFormat('en-US', {
                    month: 'short',
                    day: 'numeric'
                  }).format(upcomingPendingRequest.start_date)}{' '}
                  to{' '}
                  {new Intl.DateTimeFormat('en-US', {
                    month: 'short',
                    day: 'numeric'
                  }).format(upcomingPendingRequest.end_date)}
                </>
              ) : (
                'No pending vacation'
              )}
            </div>
          </div>

          <div className="stat">
            <div className="stat-title">Days Left</div>
            <div
              className={cx('stat-value', {
                'text-blue': user?.days_left && user.days_left >= 5,
                'text-red': user?.days_left && user.days_left < 5,
                'text-medium-gray dark:text-medium-gray-2': user?.days_left === 0 || user?.days_left === undefined
              })}
            >
              {user?.days_left}
            </div>
            <div className="stat-desc">
              {user?.days_assigned && user.days_left && user.days_left > 0 ? (
                <>
                  Of {user?.days_assigned} ({((user?.days_left / user?.days_assigned) * 100).toFixed(0)}%)
                </>
              ) : (
                'No days left'
              )}
            </div>
          </div>

          <div className="stat">
            <div className="stat-title">Pending Requests</div>
            <div
              className={cx('stat-value', {
                'text-blue': pendingRequestsCount > 0,
                'text-medium-gray dark:text-medium-gray-2': pendingRequestsCount <= 0
              })}
            >
              {pendingRequestsCount}
            </div>
            <div className="stat-desc">
              {upcomingPendingRequest ? <>{daysLeftForResponse} days left for response</> : 'No pending vacation'}
            </div>
          </div>
        </div>
      </div>
      <div className="mx-4 mb-4 flex w-full flex-wrap items-center justify-center gap-16 text-sm md:mb-0 lg:mx-0 lg:gap-2">
        <div>
          <div className="font-bold">Personal details</div>
          {user?.date_of_birth && <div>Date of birth: {format(user?.date_of_birth, 'dd-MM-yyyy')}</div>}
          <div>Email: {user?.email}</div>
          {user?.phone_number && <div>Phone: {user?.phone_number}</div>}
        </div>
        <LineWithDots className="hidden grow lg:block" />
        <div>
          <div className="font-bold">Job details</div>
          {user?.job_title && <div>Position: {user.job_title}</div>}
          {user?.teams && <div>Current teams: {user.teams}</div>}
        </div>
        <LineWithDots className="hidden grow lg:block" />
        <div>
          <div className="font-bold">Free days</div>
          {user?.days_assigned && <div>Days of vacation this year: {user.days_assigned}</div>}
          {user?.days_left && <div>Days left in this year: {user.days_left}</div>}
          {daysUsed !== 0 && <div>Used days: {daysUsed}</div>}
        </div>
      </div>
      <div className="hidden md:block">
        <RequestTable filteredRequests={thisYearRequests} user={user} all={false} simple />
      </div>
    </div>
  )
}
export default Dashboard
