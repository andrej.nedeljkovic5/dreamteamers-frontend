[run eslint --fix on save](https://www.jetbrains.com/help/webstorm/eslint.html#ws_eslint_configure_run_eslint_on_save)

### Styling

- tailwind docs: https://tailwindcss.com/docs/utility-first
- to apply styles for dark mode use directive `dark:` as the first directive (chain directives like `dark:md:hover:bg-white`)
- to use custom classes with tailwind modifiers, add class to [index.css](src/index.css)
- to add custom theme values, add them to [tailwind.config.js](tailwind.config.js) ([docs](https://tailwindcss.com/docs/adding-custom-styles#customizing-your-theme))

### Routing

- create routes with jsx: [link](https://reactrouter.com/en/main/utils/create-routes-from-elements#createroutesfromelements)
- guarding protected routes: [link](https://www.robinwieruch.de/react-router-private-routes/)

### Redux state management

- [docs](https://redux-toolkit.js.org/tutorials/typescript)

### form handling

- [docs](https://formik.org/docs/overview)
- we used the `useFormik` [hook](https://formik.org/docs/api/useFormik)
