module.exports = {
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        extensions: ['.ts', '.tsx', '.js'],
        root: "./src"
      }
    ],
  ]
};
